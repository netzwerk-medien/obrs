# OBRS*lang* – OBRS Language File

A simple definiton to manage strings

## File System Location


```

    /[base]/[language_country]/[set].obrslang

```

* base: Path to Strings
* language: ISO 639-1 code. If not available: ISO 639-3. Use lowercase letters.
* country: ISO 3166-1 alpha-2 code. Use ISO 3166-1 alpha-3 code instead, if ISO 639-3 is used as language code. Use uppercase letters.
* set: Custom name for a string set.


de_DE example:

```

    /opt/project/i18n/de_DE/main.obrslang

```

## OBRS Language File

File definition

### Strings

Strings are inserted using a three line format:

```

LINE 1: ID
LINE 2: String
LINE 3: Empty Line

```

* LINE 1: ID. Suggested characters: 0-9, A-Z and underscore. Suggested format: FIRSTLEVEL_SECONDLEVEL_THIRDLEVEL_[…nth]LEVEL
* LINE 2: Valid UTF-8 string
* LINE 3: Empty Line

de_DE example:

```

EXAMPLE_TITLE
Beispiel

EXAMPLE_CONTENT_HEADER
Beispiel-Überschrift

EXAMPLE_CONTENT_TEXT
Beispielhafter Text


```

### String placeholders

Placeholders may be numbers or strings.

#### One number and / or one string with one or more occurrences

```

My name is %s. My birthday is in %d days!. Imagine, only %d days!

```

* %d: Number placeholder.
* %s: String placeholder.

#### Multiple numbers and / or multiple strings with one or more occurrences each

```

My name is %1$s. I know you are %2$s. My birthday is in %1$d days!. Imagine, only %1$d days! Then I am %2$d years old. Did you invite %3$s?

```

* %1$d: First number placeholder.
* %2$d: Second number placeholder.
* %1$s: First string placeholder.
* %2$s: Second string placeholder.
* %3$s: Third string placeholder.


**NOTE: %d is handled as an alias to %1$d and %s is handled as an alias to %1$s.**

**NOTE: Numbers and Strings are counted seperatly. They are also handeled independently.**

### Comments

Comments are inserted using a two line format:

```

LINE 1: Comment 1st line
LINE 1.1: Comment 2nd line
LINE 1.2: Comment 3rd line
LINE 1…: Comment …nth line
LINE 2: Empty Line

```

* LINE 1: Each line of the comment starts with character '#'
* LINE 2: Empty Line

de_DE example:

```

# Kommentar

STRING_ID
Übersetzung

…

```

## Common Errors

### Multiple Strings without empty line specified

Every line between an ID and a new line is considered as a new translation and will replace the previous line. Multiple line strings are not supported.

### Empty String Set

Not valid, should be handled as error.

### No string specified for valid ID / application requests not existing string (e.g. for a specific language)

Should produce an empty string.

### Comment at string postion

Handled as string.

### No new line after comment

The following line is handled as new ID

### Application provides more placeholders than given in language file

Any placeholder provided by application, but not given in language file should be ignored.

### More placeholders given in language file than used in application

Unused placeholders should be removed.