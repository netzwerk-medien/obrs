#!/bin/bash
OBRS_BASE_PATH="/opt/obrsapp"
sudo mkdir $OBRS_BASE_PATH/logs
sudo mkdir $OBRS_BASE_PATH/shell_media/records
sudo mkdir $OBRS_BASE_PATH/shell_media/trash
sudo mkdir $OBRS_BASE_PATH/shell_media/thumbnails
sudo mkdir $OBRS_BASE_PATH/shell_media/user_backgrounds_tmp
sudo mkdir $OBRS_BASE_PATH/shell_media/user_backgrounds
sudo mkdir $OBRS_BASE_PATH/shell_media/user_backgrounds_crop
sudo mkdir $OBRS_BASE_PATH/shell_media/user_backgrounds_trash
sudo mkdir $OBRS_BASE_PATH/web_backend/config
sudo mkdir $OBRS_BASE_PATH/web_backend/database
sudo touch $OBRS_BASE_PATH/web_backend/database/obrs.sqlite.db
sudo chown -R kiosk:kiosk $OBRS_BASE_PATH/*
sudo chmod -R 600 $OBRS_BASE_PATH/shell_media/*
sudo chmod -R u+X $OBRS_BASE_PATH/shell_media/*
sudo chmod -R 700 $OBRS_BASE_PATH/shell_interface/*
