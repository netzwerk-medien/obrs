#!/usr/bin/php
<?php
/**
 * This is the database and folder update script
 */

/**
 * OBRS Web Backend Path
 */
define ("OBRS_BASE_PATH", '/opt/obrsapp/web_backend');

$pdo = new PDO("sqlite:".OBRS_BASE_PATH."/database/obrs.sqlite.db");

// 2021-08-30 UPDATE

$pdo->exec('ALTER TABLE sessions ADD sessiontimeend TIMESTAMP DEFAULT NULL');
print_r($pdo->errorInfo());

$pdo->exec('CREATE TABLE IF NOT EXISTS users (
                        auto_id INTEGER PRIMARY KEY,
                        userid TEXT UNIQUE NOT NULL,
                        password TEXT NOT NULL
                      )');
print_r($pdo->errorInfo());

$pdo->exec('CREATE TABLE IF NOT EXISTS admins (
                        auto_id INTEGER PRIMARY KEY,
                        userid TEXT UNIQUE NOT NULL,
						auth_level INTEGER NOT NULL
                      )');
print_r($pdo->errorInfo());

mkdir(OBRS_BASE_PATH . '/../logs');
exit;

// 2020-08-04 UPDATE

$pdo->exec('ALTER TABLE sessions ADD session_active INTEGER NOT NULL DEFAULT 0');
print_r($pdo->errorInfo());

$pdo->exec('CREATE TABLE IF NOT EXISTS backgrounds (
                        background_id  INTEGER PRIMARY KEY,
                        userid TEXT NOT NULL,
                        filename TEXT UNIQUE NOT NULL,
                        state INTEGER NOT NULL,
                        backgroundsavetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                      )');
print_r($pdo->errorInfo());

mkdir(OBRS_BASE_PATH . '/../shell_media/user_backgrounds_tmp');
mkdir(OBRS_BASE_PATH . '/../shell_media/user_backgrounds');
mkdir(OBRS_BASE_PATH . '/../shell_media/user_backgrounds_crop');
mkdir(OBRS_BASE_PATH . '/../shell_media/user_backgrounds_trash');
