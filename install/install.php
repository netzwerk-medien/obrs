#!/usr/bin/php
<?php
/**
 * This is the database init script
 */

/**
 * OBRS Web Backend Path
 */
define ("OBRS_BASE_PATH", '/opt/obrsapp/web_backend');
$pdo = new PDO("sqlite:".OBRS_BASE_PATH."/database/obrs.sqlite.db");

$pdo->exec('CREATE TABLE IF NOT EXISTS sessions (
                        session_id   INTEGER PRIMARY KEY,
                        userid TEXT NOT NULL,
                        session_active INTEGER NOT NULL DEFAULT 0,
                        sessiontime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        sessiontimeend TIMESTAMP DEFAULT NULL
                      )');

$pdo->exec('CREATE TABLE IF NOT EXISTS videos (
                        video_id  INTEGER PRIMARY KEY,
                        session_id  INTEGER,
                        userid TEXT NOT NULL,
                        filename TEXT UNIQUE NOT NULL,
                        videosavetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                      )');

$pdo->exec('CREATE TABLE IF NOT EXISTS backgrounds (
                        background_id  INTEGER PRIMARY KEY,
                        userid TEXT NOT NULL,
                        filename TEXT UNIQUE NOT NULL,
                        state INTEGER NOT NULL,
                        backgroundsavetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                      )');

$pdo->exec('CREATE TABLE IF NOT EXISTS users (
                        auto_id INTEGER PRIMARY KEY,
                        userid TEXT UNIQUE NOT NULL,
                        password TEXT NOT NULL
                      )');

$pdo->exec('CREATE TABLE IF NOT EXISTS admins (
                        auto_id INTEGER PRIMARY KEY,
                        userid TEXT UNIQUE NOT NULL,
						auth_level INTEGER NOT NULL
                      )');

print_r($pdo->errorInfo());


$stmt = $pdo->query("SELECT name
                                   FROM sqlite_master
                                   WHERE type = 'table'
                                   ORDER BY name");


$stmt = $pdo->query("SELECT * FROM sessions ORDER BY sessiontime DESC");
echo '<h1>Sessions</h1>';
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
{
    echo $row['session_id'] . '|' . $row['userid'] . '|' . $row['sessiontime'] . "<br/>";
}

$stmt = $pdo->query("SELECT * FROM videos ORDER BY videosavetime DESC");
echo '<h1>Videos</h1>';
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
{
    echo $row['video_id'] . '|' . $row['session_id'] . '|' . $row['userid'] . '|' . $row['filename'] . '|' . $row['videosavetime'] . "<br/>";
}

$stmt = $pdo->query("SELECT * FROM backgrounds ORDER BY backgroundsavetime DESC");
echo '<h1>Backgrounds</h1>';
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
{
    echo $row['background_id'] . '|' . $row['userid'] . '|' . $row['filename'] . '|' . $row['backgroundsavetime'] . "<br/>";
}
