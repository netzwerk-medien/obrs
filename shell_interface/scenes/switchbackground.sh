#!/bin/bash

###
# Set selected background
###

# Background path (as configured in OBS-Studio software)
path=$(realpath "$(dirname "$0")/../../")
cd "$path"

# If chosen background does not exist, set default value
background="backgrounds/default"

# Set custom background false
background_user=0

while getopts ":b:u:" opt; do
	case $opt in
	b)
		background_arg=$(echo "$OPTARG" | sed "s/[^a-zA-Z0-9_]//g")
		;;
	u)
		background_user=$(echo "$OPTARG" | sed "s/[^0-9]//g")
		;;
	esac
done

# Check custom background
if (($background_user == 1)); then
	if [[ -f shell_media/user_backgrounds/$background_arg.png ]]; then
		background="user_backgrounds/$background_arg"
	fi
else
	if [[ -f shell_media/backgrounds/private_branding/$background_arg.png ]]; then
		background="backgrounds/private_branding/$background_arg"
	elif [[ -f shell_media/backgrounds/$background_arg.png ]]; then
		background="backgrounds/$background_arg"
	fi
fi

# Set background
cp "shell_media/$background.png" shell_media/OBRS_Background_BETA.png && exit 0 || exit 1
