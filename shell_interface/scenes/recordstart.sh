#!/bin/bash

###
# Start Recording (Hotkey I)
###

xdotool search --sync --onlyvisible --name "OBS" key ctrl+i || exit 1
