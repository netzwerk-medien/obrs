#!/bin/bash

###
# Clean-up script
###

# Set DMX-values
ola_set_dmx -u 1 -d 0,0,0,0,0,0,0,0,0,0,0,0

# Set default recording source
# This command may fail if multiple USB recording devices are plugged in
pacmd set-default-source $(pacmd list-sources | grep -Ei 'name:.*input.*usb.*multichannel' | cut -d ' ' -f2 | tr -d '[<|>]') &>/dev/null

exit 0
