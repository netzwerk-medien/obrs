#!/bin/bash

###
# Stop Recording (Hotkey J)
###

xdotool search --sync --onlyvisible --name "OBS" key ctrl+j || exit 1
