#!/bin/bash

###
# Set selected scenario
###

# Script path
path=$(realpath $(dirname "$0"))
cd "$path"

# Set room, expert and key setting
while getopts ":d:k:" opt; do
	case $opt in
	d)
		dmx=$(echo "$OPTARG" | sed "s/[^0-9,\-]//g")
		;;
	k)
		key=$(echo "$OPTARG" | sed "s/[^a-zA-Z0-9]//g")
		;;
	*)
		echo "Unbekannte Option gesetzt."
		;;
	esac
done

# Test if variables are set
[ -z "$dmx" ] || [ -z "$key" ] && exit 3

# Select scenario (as configured in OBS-Studio software)
xdotool search --sync --onlyvisible --name "OBS" key ctrl+$key || exit 1

# Lights
if [[ "$dmx" != "-1" ]]; then
	ola_set_dmx -u 1 -d "$dmx" || exit 2
fi
