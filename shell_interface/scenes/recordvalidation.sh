#!/bin/bash

###
# Validate Recording (Check OBS Log if record started)
###

while getopts "f:a:" opt; do
	case $opt in
	f)
		greptext="$OPTARG"
		;;
	a)
		ignoretext="$OPTARG"
		;;
	esac
done

output=$(grep -v "$ignoretext" "/home/$(whoami)/.config/obs-studio/logs/$(ls -t /home/$(whoami)/.config/obs-studio/logs/ | head -1)" | tail -1 | grep "$greptext")
[[ ! -z "$output" ]] && exit 0 || exit 1
