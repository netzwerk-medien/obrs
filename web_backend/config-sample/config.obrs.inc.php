<?php

//Servername
define("OBRS_LINK", 'https://example.com');

//Document camera in studio?
define("OBRS_DOCUMENT_CAM", false);
//Only one tablet (setup and room combined)?
define("OBRS_SINGLE_IPAD", false);
//Disable light control by studio
define("OBRS_NO_LIGHTS", false);
//DMX light values
define("OBRS_DMX_VALUES", "255");
//Allow green screen scene (could have problems with keying)
define("OBRS_SUPPORT_GREEN_SCREEN", false);

//Map JavaScript values for scene to OBS hotkey value for scene
define("OBRS_PROFILE_KEYS", array("10" => "r", "11" => "t", "12" => "e", "20" => "p", "21" => "o", "22" => "u", "30" => "z"));

//Number of department backgrounds
define("OBRS_DEPARTMENT_BACKGROUND_COUNT", 0);

//IPs that are allowed to control this studio (tablets)
define("OBRS_CLIENT_IPS", array('/^192\.168\.0\.[0-9]{1,3}$/'));

//Set login system
define("OBRS_LOGIN_HANDLER_SHIB", 0);
define("OBRS_LOGIN_HANDLER_LDAP", 1);
define("OBRS_LOGIN_HANDLER_INTERNAL", 2);
define("OBRS_LOGIN_HANDLER", OBRS_LOGIN_HANDLER_SHIB);

//Internal auth usernames: Allowed pattern
define("OBRS_INTERNAL_AUTH_LOGIN_USERNAME_PATTERN", "/[^0-9A-Za-z]/");

//LDAP specific configuration
define("OBRS_LDAP_HOST", "ldaps://ldap.example.com");
define("OBRS_LDAP_LOGIN_USER", "%1"); //%1 is a placeholder for real username
define("OBRS_LDAP_LOGIN_USERNAME_PATTERN", "/[^0-9A-Za-z]/"); //pattern for real username
define("OBRS_LDAP_USER", "myadminusername");
define("OBRS_LDAP_PW", "myadminpassword");
define("OBRS_LDAP_LOGIN_SEARCH_USER_PATTERN", "%1"); //%1 is a placeholder for real username
define("OBRS_LDAP_BASE", "basedn");
define("OBRS_LDAP_ATTR_ID", "id");
define("OBRS_LDAP_ATTR_NAME", "name");
define("OBRS_LDAP_ATTR_MAIL", "mail");

//Login URLs and strings: Setup
define("OBRS_SHIB_LOGIN_URL", OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL ? OBRS_LINK . "/internal_auth/login?dest=" . rawurlencode('/setup') : (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP ? OBRS_LINK . "/ldap/login?dest=" . rawurlencode('/setup') : OBRS_LINK . '/Shibboleth.sso/Login?target=' . urlencode(OBRS_LINK . '/setup')));
define("OBRS_SHIB_LOGOUT_URL", OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL ? OBRS_LINK . "/internal_auth/logout?dest=" . rawurlencode('/setup') : (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP ? OBRS_LINK . "/ldap/logout?dest=" . rawurlencode('/setup') : OBRS_LINK . '/Shibboleth.sso/Logout?return=' . urlencode(OBRS_LINK . '/setup/index.php')));
define("OBRS_SHIB_HEAD_TITLE", "OBRS-Login");
define("OBRS_SHIB_BODY_TITLE", "Willkommen im OBRS");
define("OBRS_SHIB_MESSAGE", array("Bitte melden Sie sich mit einem Klick auf LOGIN mit Ihrem Account an.", "Gemäß der DSGVO erklären Sie sich mit der Nutzung dieses Studios bereit, dass die Daten der Aufnahmen auf digitalen Datenspeichern abgelegt und verarbeitet werden, um Sie Ihnen zum Download zur Verfügung zu stellen."));

//Login URLs and strings: Userarea
define("OBRS_SHIB_LOGIN_URL_USER_AREA", OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL ? OBRS_LINK . "/internal_auth/login?dest=" . rawurlencode('/userarea') : (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP ? OBRS_LINK . "/ldap/login?dest=" . rawurlencode('/userarea') : OBRS_LINK . '/Shibboleth.sso/Login?target=' . urlencode(OBRS_LINK . '/userarea')));
define("OBRS_SHIB_LOGOUT_URL_USER_AREA", OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL ? OBRS_LINK . "/internal_auth/logout?dest=" . rawurlencode('/userarea') : (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP ? OBRS_LINK . "/ldap/logout?dest=" . rawurlencode('/userarea') : OBRS_LINK . '/Shibboleth.sso/Logout?return=' . urlencode(OBRS_LINK . '/userarea/index.php')));
define("OBRS_SHIB_HEAD_TITLE_USER_AREA", "Nutzerbereich");
define("OBRS_SHIB_BODY_TITLE_USER_AREA", "Willkommen im Nutzerbereich");
define("OBRS_SHIB_MESSAGE_USER_AREA", array("Bitte melden Sie sich mit einem Klick auf LOGIN mit Ihrem Account an."));

//Login URLs and strings: Adminarea
define("OBRS_SHIB_LOGIN_URL_ADMIN_AREA", OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL ? OBRS_LINK . "/internal_auth/login?dest=" . rawurlencode('/adminarea') : (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP ? OBRS_LINK . "/ldap/login?dest=" . rawurlencode('/adminarea') : OBRS_LINK . '/Shibboleth.sso/Login?target=' . urlencode(OBRS_LINK . '/adminarea')));
define("OBRS_SHIB_LOGOUT_URL_ADMIN_AREA", OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL ? OBRS_LINK . "/internal_auth/logout?dest=" . rawurlencode('/adminarea') : (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP ? OBRS_LINK . "/ldap/logout?dest=" . rawurlencode('/adminarea') : OBRS_LINK . '/Shibboleth.sso/Logout?return=' . urlencode(OBRS_LINK . '/adminarea/index.php')));
define("OBRS_SHIB_HEAD_TITLE_ADMIN_AREA", "Adminbereich");
define("OBRS_SHIB_BODY_TITLE_ADMIN_AREA", "Willkommen im Adminbereich");
define("OBRS_SHIB_MESSAGE_ADMIN_AREA", array("Bitte melden Sie sich mit einem Klick auf LOGIN mit Ihrem Admin-Account an."));

//Shibboleth specific configuration
define("OBRS_SHIB_ID_PROVIDER", 'https://example.com/idp/shibboleth');
define("OBRS_SHIB_AFFILIATIONS", array('someone@example.com', 'staff@example.com'));
define("OBRS_SHIB_AFFILIATION_ERROR", 'Wir konnten Sie nicht als Angehörigen erkennen.');
define("OBRS_SHIB_MAIL_ERROR", 'Wir konnten Ihre E-Mail Adresse nicht abrufen.');
define("OBRS_SHIB_NAME_ERROR", 'Wir konnten Ihren Namen nicht abrufen.');
define("OBRS_SHIB_LOGIN_ERROR", 'Anmeldefehler.');

//Inactivity time for automatic logout
define("OBRS_AUTO_LOGOUT_TIME", 43200 /* 12 hours */);

//Amount of days for user videos to display
define("OBRS_VIDEO_DISPLAY_TIME", 14);
//Video file type
define("OBRS_VIDEO_FILE_EXTENSION", 'mp4');
define("OBRS_VIDEO_FILE_MIME", 'video/mp4');

//Validation of recording
define("OBRS_VALIDATE_RECORDING", false);
define("OBRS_VALIDATE_RECORDING_START", " Writing file");
define("OBRS_VALIDATE_RECORDING_IGNORE", "pulse-am\|audio");

//OBRS Web Backend file path
if (!defined("OBRS_BASE_PATH")) {
        define("OBRS_BASE_PATH", '/opt/obrsapp/web_backend');
}

//File path for shell media
define("OBRS_SHELL_MEDIA_PATH", OBRS_BASE_PATH . '/../shell_media');
define("OBRS_SHELL_MEDIA_PATH_RECORDS", OBRS_SHELL_MEDIA_PATH . '/records');
define("OBRS_SHELL_MEDIA_PATH_RECORDS_TRASH", OBRS_SHELL_MEDIA_PATH . '/trash');
define("OBRS_SHELL_MEDIA_PATH_BACKGROUNDS", OBRS_SHELL_MEDIA_PATH . '/backgrounds');
define("OBRS_SHELL_MEDIA_PATH_THUMBS", OBRS_SHELL_MEDIA_PATH . '/thumbnails');
define("OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_TMP", OBRS_SHELL_MEDIA_PATH . '/user_backgrounds_tmp');
define("OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_FINAL", OBRS_SHELL_MEDIA_PATH . '/user_backgrounds');
define("OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_CROP", OBRS_SHELL_MEDIA_PATH . '/user_backgrounds_crop');
define("OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_TRASH", OBRS_SHELL_MEDIA_PATH . '/user_backgrounds_trash');

//File path for shell scripts
define("OBRS_SHELL_INTERFACE_PATH", OBRS_BASE_PATH . '/../shell_interface');
define("OBRS_SHELL_INTERFACE_PATH_SCENES", OBRS_SHELL_INTERFACE_PATH . '/scenes');

//Log path for internal logging
define("OBRS_LOG_PATH", OBRS_BASE_PATH . '/../logs');

//Links for user area
define("OBRS_LINK_USER_AREA", OBRS_LINK . '/userarea');
define("OBRS_LINK_VIDEOARCHIVE", OBRS_LINK . '/userarea/videoarchive');
define("OBRS_LINK_HELP", 'https://example.com');

//Links for admin area
define("OBRS_LINK_ADMIN_AREA", OBRS_LINK . '/adminarea');

//Configure mail at user login in studio
define("OBRS_MAIL", 'obrs@example.com');
define("OBRS_MAIL_FROM", 'One Button Recording Studio <' . OBRS_MAIL . '>');
define("OBRS_MAIL_REPLYTO", OBRS_MAIL);
define("OBRS_MAIL_SUBJECT", 'OBRS - Ihre Videos sind abrufbar');
define("OBRS_MAIL_MESSAGE", 'Sehr geehrter/e OBRS-Nutzer/in,

vielen Dank für die Nutzung des OBRS (One Button Recording Studios).
Ihre Videos sind ab sofort ' . OBRS_VIDEO_DISPLAY_TIME . ' Tage lang unter folgender Adresse abrufbar: ' . OBRS_LINK_VIDEOARCHIVE . '

Falls bei der Benutzung des Studios Fragen aufgekommen sind oder Sie uns Feedback/Verbesserungsvorschläge zukommen lassen wollen, wenden Sie sich gerne an ' . OBRS_MAIL . '.

Weitere Infos zum Studio finden Sie hier: ' . OBRS_LINK_HELP . '

Wir wünschen Ihnen viel Erfolg mit den produzierten Videos,
Ihr OBRS-Team');

//Translation stings config
define("OBRS_I18N_ERROR", 'Could not load one or more language files.');
define("OBRS_I18N_LOCALE", 'de_DE');
define("OBRS_I18N_ENCODING", 'UTF-8');
define("OBRS_I18N_PATH", OBRS_BASE_PATH . '/i18n');
define("OBRS_I18N_SET_GENERAL", 'OBRS');
define("OBRS_I18N_SET_CONNECTOR", 'OBRS_CONNECTOR');
define("OBRS_I18N_SET_ROOM", 'OBRS_ROOM');
define("OBRS_I18N_SET_SETUP", 'OBRS_SETUP');
define("OBRS_I18N_SET_USER_AREA", 'OBRS_USER_AREA');
define("OBRS_I18N_SET_USER_AREA_PICS", 'OBRS_USER_AREA_PICS');
define("OBRS_I18N_SET_ADMIN_AREA", 'OBRS_ADMIN_AREA');
define("OBRS_I18N_SET_LDAP", 'OBRS_LDAP');
define("OBRS_I18N_SET_INTERNAL_AUTH", 'OBRS_INTERNAL_AUTH');

//CSS and JS config
define("OBRS_WEB_CSS_PATH", '/css');
define("OBRS_WEB_CSS_MAIN", OBRS_WEB_CSS_PATH . '/main.css');
define("OBRS_WEB_CSS_COMMON", OBRS_WEB_CSS_PATH . '/common.css');
define("OBRS_WEB_CSS_SETUP", OBRS_WEB_CSS_PATH . '/setup.css');
define("OBRS_WEB_CSS_ROOM", OBRS_WEB_CSS_PATH . '/room.css');
define("OBRS_WEB_CSS_USER_AREA", OBRS_WEB_CSS_PATH . '/userarea.css');
define("OBRS_WEB_CSS_VIDEOARCHIVE", OBRS_WEB_CSS_PATH . '/userarea-videoarchive.css');
define("OBRS_WEB_CSS_USER_AREA_PICS_GRID", OBRS_WEB_CSS_PATH . '/userarea-pics-grid.css');
define("OBRS_WEB_JS_PATH", '/js');
define("OBRS_WEB_JS_COMMON", OBRS_WEB_JS_PATH . '/common.js');
define("OBRS_WEB_JS_SCREENSAVER", OBRS_WEB_JS_PATH . '/screensaver.js');
define("OBRS_WEB_JS_SETUP", OBRS_WEB_JS_PATH . '/setup.js');
define("OBRS_WEB_JS_ROOM", OBRS_WEB_JS_PATH . '/room.js');
define("OBRS_WEB_JS_USER_AREA", OBRS_WEB_JS_PATH . '/userarea.js');

//User area background links
define("OBRS_WEB_PATH_BACKGROUNDS_SETUP", OBRS_LINK . '/bild');
define("OBRS_WEB_PATH_BACKGROUNDS_USER_FINAL", OBRS_LINK_USER_AREA . '/hintergrund/bild');
define("OBRS_WEB_PATH_BACKGROUNDS_USER_CROP", OBRS_LINK_USER_AREA . '/hintergrund/bild');

//User area background config
define("OBRS_USER_AREA_PICS_WIDTH", 1920);
define("OBRS_USER_AREA_PICS_HEIGHT", 1080);
define("OBRS_USER_AREA_PICS_RATIO", '16:9');
define("OBRS_USER_AREA_PICS_MAX_SIZE", 5);
define("OBRS_USER_AREA_PICS_MAX_NUMBER", 6);

//User area background states
define("OBRS_USER_AREA_PICS_STATE_TMP", 0);
define("OBRS_USER_AREA_PICS_STATE_FINAL", 1);
define("OBRS_USER_AREA_PICS_STATE_CROP", 2);
define("OBRS_USER_AREA_PICS_STATE_TRASH", 3);

//Admin area config
define("OBRS_SUPER_ADMIN", 'admin@domain');
define("OBRS_ADMIN_LEVEL_GENERAL", 1);
define("OBRS_ADMIN_LEVEL_VIDEO_MGR", 100);
define("OBRS_ADMIN_LEVEL_VIEW_LOGS", 200);
define("OBRS_ADMIN_LEVEL_ADD_ADMIN", 300);
define("OBRS_ADMIN_LEVEL_CONTROL_STUDIO", 400);
define("OBRS_ADMIN_LEVEL_DB", 500);
