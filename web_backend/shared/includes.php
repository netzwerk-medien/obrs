<?php

/**
 * Include required files
 */

/**
 * Include config
 */
require_once(OBRS_BASE_PATH .  "/config/config.obrs.inc.php");

/**
 * Include headers
 */
require_once(OBRS_BASE_PATH .  "/shared/headers.php");

/**
 * Include functions
 */
require_once(OBRS_BASE_PATH .  "/shared/functions.php");

/**
 * Include database connection
 */
require_once(OBRS_BASE_PATH .  "/class/Sqlite.php");

/**
 * Include string finder
 */
require_once(OBRS_BASE_PATH .  "/class/StringFinder.php");
