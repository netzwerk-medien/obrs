<?php

/**
 * General functions
 */

namespace NetzwerkMedienObrs;

/**
 * Generate thumbnail for the latest recorded video
 * @return boolean
 */
function generateVideoPreview()
{
	$filename = (scandir(OBRS_SHELL_MEDIA_PATH_RECORDS, SCANDIR_SORT_DESCENDING))[0];
	if (substr($filename, -3) == OBRS_VIDEO_FILE_EXTENSION) {
		$first = substr($filename, 0, -4) . '.jpg';
		$cmd = sprintf("ffmpeg -ss 0.5 -i %s -t 1 -s 192x108 -f image2 %s", OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $filename, OBRS_SHELL_MEDIA_PATH_THUMBS . '/' . $first);
		shell_exec($cmd);
		return true;
	}
	return false;
}
/**
 * Get latest recorded video
 * @return string
 */
function getLatestVideo()
{
	$files = scandir(OBRS_SHELL_MEDIA_PATH_RECORDS, SCANDIR_SORT_DESCENDING);
	return $files[0];
}

/**
 * Send Video Link To User
 * @param string $email eMail Address
 * @return boolean
 */
function sendVideoLink($email)
{
	$from = OBRS_MAIL_FROM;
	$replyto = OBRS_MAIL_REPLYTO;
	$subject = OBRS_MAIL_SUBJECT;
	$message = OBRS_MAIL_MESSAGE;

	$header = 'Content-Type: text/plain; charset=UTF-8' . "\r\n" .
		'From: ' . $from . "\r\n" .
		'Reply-To: ' . $replyto . "\r\n" .
		'Return-Path: ' . $replyto . "\r\n" .
		'X-Sender: ' . $replyto;
	return mail($email, $subject, $message, $header);
}

/**
 * Validate Client IP
 * @param string $ip current ip
 * @param array $patterns valid ips
 * @return boolean
 */
function validateClientIP($ip, $patterns)
{
	foreach ($patterns as $pattern) {
		if (preg_match($pattern, $ip) === 1) {
			return true;
		}
	}
	return false;
}

/**
 * Check Super Admin
 * @param string $email mail
 * @return boolean
 */
function checkSuperAdmin($email)
{
	if (defined("OBRS_SUPER_ADMIN") && !empty(OBRS_SUPER_ADMIN)) {
		return $email === OBRS_SUPER_ADMIN;
	}
	return false;
}

/**
 * Check Admin
 * @param string $email mail
 * @param int $level level
 * @return boolean
 */
function checkAdmin($email, $level)
{
	$pdo = new \NetzwerkMedienObrs\Sqlite;
	$pdo->connect();
	return \netzwerkMedienObrs\checkSuperAdmin($email) || ($pdo->isAdmin($email) && $pdo->adminAuthLevel($email) > 0 && $pdo->adminAuthLevel($email) >= $level);
}
