<?php

/**
 * Video
 */

/**
 * Does file exist
 */
if (!file_exists(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$video")) {
    http_response_code(404);
    exit;
}

/**
 * Read video
 */
$handle = fopen(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$video", 'r');
if ($handle === false) {
    http_response_code(500);
    exit;
}
$filesize = filesize(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$video");
$length = $filesize;
$start = 0;
$end = $filesize - 1;
if (isset($_SERVER['HTTP_RANGE'])) {
    preg_match('/bytes=(\d+)-(\d+)?/', $_SERVER['HTTP_RANGE'], $matches);
    $start = intval($matches[1]);
    $end = isset($matches[2]) && !empty($matches[2]) ? intval($matches[2]) : $end;
    $length = $end - $start + 1;
    if ($start > $end || $start >= $filesize || $end >= $filesize) {
        header('HTTP/1.1 416 Requested Range Not Satisfiable');
        exit;
    }
    header('HTTP/1.1 206 Partial Content');
    header('Content-Range: bytes ' . $start . '-' . $end . '/' . $filesize);
    header("Accept-Ranges: bytes");
} else {
    http_response_code(200);
}
header('Content-Type: ' . OBRS_VIDEO_FILE_MIME);
header('Content-Length: ' . $length);
fseek($handle, $start);

while (($reading = fgets($handle, 4096)) !== false) {
    echo $reading;
}

fclose($handle);
