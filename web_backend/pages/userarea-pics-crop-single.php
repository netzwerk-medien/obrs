<?php
$pdo = new \NetzwerkMedienObrs\Sqlite;
$pdo->connect();

if (!isset($_POST["pic"]) || empty($_POST["pic"]) || preg_match('/^[0-9a-zA-Z]+\.png$/', $_POST["pic"]) !== 1) {
	echo $stringsUserAreaPics->getString('CROP_SINGLE_ERROR_INVALID');
	exit;
}

$id = $_POST["pic"];
$pic = OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_CROP . "/$id";

if (!$pdo->doesBackgroundBelongToUser($email, $id)) {
	echo $stringsUserAreaPics->getString('CROP_SINGLE_ERROR_NOT_FOUND');
	exit;
}

if (!file_exists($pic)) {
	echo $stringsUserAreaPics->getString('CROP_SINGLE_ERROR_NOT_FOUND');
	exit;
}
?>
<!doctype html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $stringsUserAreaPics->getString('CROP_SINGLE_NAME'); ?></title>
	<link rel="icon" type="image/x-icon" href="icon.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
	<style>
		#pics-info {
			padding: 30px;
		}

		#pics-rect {
			content: ' ';
			background: floralWhite;
			background: linear-gradient(-45deg, #fcc, #ffc, #fcc, #efc, #ccf, #fcf);
			outline: rgb(17, 0, 51) dashed 4px;
			outline-offset: -4px;
			opacity: 0.5;
			position: absolute;
			z-index: 100;
		}

		#pics input {
			background: #ddd;
			color: #001;
			border: 2px solid #001;
			border-radius: 6px;
			font-size: 1.01em;
			margin: 6px;
			padding: 6px;
		}
	</style>
	<script src="/js/userarea.js"></script>
</head>

<body>
	<?php
	$nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
	$nav->getHTML();
	?>
	<main>
		<article id="pics">
			<header>
				<h1><?php echo $stringsUserAreaPics->getString('CROP_SINGLE_NAME'); ?></h1>
			</header>
			<div id="pics-info"><?php echo $stringsUserAreaPics->getString('CROP_SINGLE_NOTE'); ?></div>
			<div>
				<div id="pics-rect"></div>
				<img id="pics-pic" src="<?php echo OBRS_WEB_PATH_BACKGROUNDS_USER_CROP . '/' . basename($pic); ?>" alt="<?php echo $stringsUserAreaPics->getString('CROP_ALT'); ?>">
				<form accept-charset="UTF-8" action="bilder.php" method="post">
					<input type="hidden" value="<?php echo basename($pic); ?>" name="pic">
					<input id="pics-left" type="hidden" value="0" name="pics-left">
					<input id="pics-top" type="hidden" value="0" name="pics-top">
					<input value="<?php echo $stringsUserAreaPics->getString('CROP_BUTTON'); ?>" type="submit">
				</form>
			</div>
		</article>
	</main>
	<script>
		function sizeOfPic(variant) {
			while (pic.offsetWidth < 0.75 * window.innerWidth && pic.offsetHeight < 0.75 * window.innerHeight) {
				width *= 1.25;
				height *= 1.25;
				if (variant == 'h') {
					pic.style.height = height + 'px';
				} else {
					pic.style.width = width + 'px';
				}
			}
			while (window.innerWidth < 1.5 * pic.offsetWidth || window.innerHeight < 2 * pic.offsetHeight) {
				width *= 0.75;
				height *= 0.75;
				if (variant == 'h') {
					pic.style.height = height + 'px';
				} else {
					pic.style.width = width + 'px';
				}
			}
			rect.style.width = width + 'px';
			rect.style.height = height + 'px';
		}

		function sizeOfPicV() {
			sizeOfPic('v');
			rect.style.top = parseInt(picRectInteract * pic.offsetTop, 10) + 'px';
		}

		function setRectPosV(diff) {
			if (diff === undefined || isNaN(parseInt(diff, 10))) {
				diff = 0;
			}
			var rectTop = parseInt(rect.style.top.replace(/[^0-9]/g, ''), 10);
			rectTop = isNaN(rectTop) ? 0 : rectTop;
			var rectTopNew = (rectTop + diff);
			picRectInteract = rectTopNew / pic.offsetTop;
			if (rectTopNew < pic.offsetTop) {
				rectTopNew = pic.offsetTop;
			} else if (rectTopNew + rect.offsetHeight > pic.offsetTop + pic.offsetHeight) {
				rectTopNew = pic.offsetTop + pic.offsetHeight - rect.offsetHeight;
			}
			rect.style.top = rectTopNew + 'px';
			document.querySelector("#pics-top").value = parseInt(<?php echo OBRS_USER_AREA_PICS_HEIGHT; ?> * (rectTopNew - pic.offsetTop) / height, 10);
		}

		function rectMouseDownV(e) {
			mouseTmp = e.clientY;
		}

		function rectMouseMoveV(e) {
			if (mouseTmp != 0) {
				setRectPosV(e.clientY - mouseTmp);
				mouseTmp = e.clientY;
			}
		}

		function rectMouseDownVT(e) {
			mouseTmp = e.touches[0].clientY;
		}

		function rectMouseMoveVT(e) {
			if (mouseTmp != 0) {
				setRectPosV(e.changedTouches[0].clientY - mouseTmp);
				mouseTmp = e.changedTouches[0].clientY;
			}
		}

		function sizeOfPicH() {
			sizeOfPic('h');
			rect.style.left = parseInt(picRectInteract * pic.offsetLeft, 10) + 'px';
		}

		function setRectPosH(diff) {
			if (diff === undefined || isNaN(parseInt(diff, 10))) {
				diff = 0;
			}
			var rectLeft = parseInt(rect.style.left.replace(/[^0-9]/g, ''), 10);
			rectLeft = isNaN(rectLeft) ? 0 : rectLeft;
			var rectLeftNew = (rectLeft + diff);
			picRectInteract = rectLeftNew / pic.offsetLeft;
			if (rectLeftNew < pic.offsetLeft) {
				rectLeftNew = pic.offsetLeft;
			} else if (rectLeftNew + rect.offsetWidth > pic.offsetLeft + pic.offsetWidth) {
				rectLeftNew = pic.offsetLeft + pic.offsetWidth - rect.offsetWidth;
			}
			rect.style.left = rectLeftNew + 'px';
			document.querySelector("#pics-left").value = parseInt(<?php echo OBRS_USER_AREA_PICS_WIDTH; ?> * (rectLeftNew - pic.offsetLeft) / width, 10);
		}

		function rectMouseDownH(e) {
			mouseTmp = e.clientX;
		}

		function rectMouseMoveH(e) {
			if (mouseTmp != 0) {
				setRectPosH(e.clientX - mouseTmp);
				mouseTmp = e.clientX;
			}
		}

		function rectMouseDownHT(e) {
			mouseTmp = e.touches[0].clientX;
		}

		function rectMouseMoveHT(e) {
			if (mouseTmp != 0) {
				setRectPosH(e.changedTouches[0].clientX - mouseTmp);
				mouseTmp = e.changedTouches[0].clientX;
			}
		}

		function rectMouseUp(e) {
			if (mouseTmp > 0) {
				mouseTmp = 0;
			}
		}

		var width = <?php echo OBRS_USER_AREA_PICS_WIDTH; ?>;
		var height = <?php echo OBRS_USER_AREA_PICS_HEIGHT; ?>;
		var rect = document.querySelector('#pics-rect');
		var pic = document.querySelector('#pics-pic');
		var picRectInteract = 1;
		var mouseTmp = 0;

		window.addEventListener('load', function() {
			if (pic.offsetWidth / pic.offsetHeight < width / height) {
				sizeOfPicV();
				setRectPosV();
				window.addEventListener('resize', sizeOfPicV);
				window.addEventListener('resize', setRectPosV);
				rect.addEventListener('mousedown', rectMouseDownV);
				rect.addEventListener('mousemove', rectMouseMoveV);
				rect.addEventListener('touchstart', rectMouseDownVT);
				rect.addEventListener('touchmove', rectMouseMoveVT);
			} else {
				sizeOfPicH();
				setRectPosH();
				window.addEventListener('resize', sizeOfPicH);
				window.addEventListener('resize', setRectPosH);
				rect.addEventListener('mousedown', rectMouseDownH);
				rect.addEventListener('mousemove', rectMouseMoveH);
				rect.addEventListener('touchstart', rectMouseDownHT);
				rect.addEventListener('touchmove', rectMouseMoveHT);
			}
			rect.addEventListener('mouseleave', rectMouseUp);
			rect.addEventListener('mouseup', rectMouseUp);
		});
	</script>
</body>

</html>
