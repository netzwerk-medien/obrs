<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $stringsAdminArea->getString('AREA_VIDEOMANAGER'); ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
    <style>
        .comfortable {
            margin: 10px;
            padding: 5px;
        }

        .table-comfortable {
            border-collapse: separate;
            border-spacing: 15px;
        }

        .success {
            color: darkgreen;
        }

        .failure {
            color: red;
        }

        .error {
            color: red;
            font-weight: 900;
            text-transform: uppercase;
            font-size: 200%;
        }
    </style>
    <script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
    <?php
    /**
     * Videomanager
     */

    $nav = new \NetzwerkMedienObrs\AdminAreaNavigation($stringsAdminArea);
    $nav->getHTML();

    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();
    if (isset($_GET["vid"]) && !empty($_GET["vid"])) {
        $vid = preg_replace("#/#", "", urldecode($_GET["vid"]));
        echo "<div><h1 class=\"comfortable\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_AUTO_TITLE', $vid) . "</h1>";
        if (!$pdo->doesVideoExist($vid) && file_exists(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$vid") && is_file(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$vid")) {
            $logins = $pdo->getSessions();
            $success = false;
            foreach ($logins as $current_login) {
                if (!empty($current_login["sessiontimeend"]) && filemtime(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$vid") > strtotime($current_login["sessiontime"]) + date("Z") && filemtime(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$vid") < strtotime($current_login["sessiontimeend"]) + date("Z")) {
                    if ($pdo->insertVideo($vid, (object) $current_login)) {
                        $success = true;
                        echo "<div class=\"comfortable success\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_AUTO_SUCCESS', $current_login["userid"]) . "</div>";
                    }
                    if (defined("OBRS_LOG_PATH")) {
                        $logfile = OBRS_LOG_PATH . "/admin_video_missing_auto.log";
                        if (!file_exists($logfile)) {
                            touch($logfile);
                        }
                        if (file_exists($logfile) && is_file($logfile)) {
                            file_put_contents($logfile, date("Y-m-d H:i:s") . ": $vid for " . $current_login["userid"] . " by $email" . PHP_EOL, FILE_APPEND);
                        }
                    }
                    break;
                }
            }
            if (!$success) {
                echo "<div class=\"comfortable failure\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_AUTO_FAILURE') . "</div>";
                usort($logins, function ($a, $b) {
                    return $a["sessiontime"] < $b["sessiontime"];
                });
                $mails = array();
                foreach ($logins as $current_login) {
                    if (!in_array($current_login["userid"], $mails)) {
                        array_push($mails, $current_login["userid"]);
                        echo "<div class=\"comfortable\"><a href=\"./?manvid=" . urlencode($vid) . "&amp;sid=" . urlencode($current_login["userid"]) . "\">" . $current_login["userid"] . " / " . $current_login["sessiontime"] . "</a></div>";
                    }
                }
            }
        } else {
            echo "<div class=\"comfortable error\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_AUTO_ERROR') . "</div>";
        }
        echo "</div>";
    } else if (isset($_GET["manvid"]) && !empty($_GET["manvid"]) && isset($_GET["sid"]) && !empty($_GET["sid"])) {
        $vid = preg_replace("#/#", "", urldecode($_GET["manvid"]));
        $sid = preg_replace("#['\";]#", "", urldecode($_GET["sid"]));
        echo "<div><h1 class=\"comfortable\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_MANUAL_TITLE', $vid) . "</h1>";
        if (!$pdo->doesVideoExist($vid) && file_exists(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$vid") && is_file(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$vid")) {
            $lastLogin = $pdo->getLastLogin($sid);
            if ($pdo->insertVideo($vid, $lastLogin)) {
                echo "<div class=\"comfortable success\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_MANUAL_SUCCESS', $current_login["userid"]) . "</div>";
                if (defined("OBRS_LOG_PATH")) {
                    $logfile = OBRS_LOG_PATH . "/admin_video_missing_manual.log";
                    if (!file_exists($logfile)) {
                        touch($logfile);
                    }
                    if (file_exists($logfile) && is_file($logfile)) {
                        file_put_contents($logfile, date("Y-m-d H:i:s") . ": $vid for $sid by $email" . PHP_EOL, FILE_APPEND);
                    }
                }
            } else {
                echo "<div class=\"comfortable failure\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_MANUAL_FAILURE', $current_login["userid"]) . "</div>";
                if (defined("OBRS_LOG_PATH")) {
                    $logfile = OBRS_LOG_PATH . "/admin_video_missing_manual_failure.log";
                    if (!file_exists($logfile)) {
                        touch($logfile);
                    }
                    if (file_exists($logfile) && is_file($logfile)) {
                        file_put_contents($logfile, date("Y-m-d H:i:s") . ": $vid for $sid by $email" . PHP_EOL, FILE_APPEND);
                    }
                }
            }
        } else {
            echo "<div class=\"comfortable error\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_MANUAL_ERROR') . "</div>";
        }
        echo "</div>";
    } else {
        $hideindb = (isset($_GET["hideindb"]) && $_GET["hideindb"] == 1);
        $files = array();
        if ($handle = opendir(OBRS_SHELL_MEDIA_PATH_RECORDS)) {
            while (false !== ($file = readdir($handle))) {
                if (is_file(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$file")) {
                    array_push($files, $file);
                }
            }
            closedir($handle);
        }
        sort($files);
        echo "<table class=\"table-comfortable\"><tr><th>" . $stringsAdminArea->getString('VIDEOMANAGER_VIDEO') . "</th><th>" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION') . "</th></tr>";
        foreach ($files as $file) {
            $exists = $pdo->doesVideoExist($file);
            if (!$exists || !$hideindb) {
                echo "<tr><td>$file</td><td>" . ($exists ? $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_DONE') : "<a href=\"?vid=" . urlencode($file) . "\">" . $stringsAdminArea->getString('VIDEOMANAGER_ALLOCATION_TRY') . "</a>") . "</td></tr>";
            }
        }
        echo "</table>";
        echo "<div class=\"comfortable\"><a href=\"?hideindb=" . intval(!$hideindb) . "\">" . $stringsAdminArea->getString($hideindb ? 'VIDEOMANAGER_HIDE_IN_DB_OFF' : 'VIDEOMANAGER_HIDE_IN_DB') . "</a></div>";
    }
    ?>
</body>

</html>
