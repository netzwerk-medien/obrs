<?php

/**
 * Picture
 */

/**
 * Does file exist
 */
if (!file_exists($file)) {
    http_response_code(404);
    exit;
}

/**
 * Read picture
 */
$handle = fopen($file, 'r');
if ($handle === false) {
    http_response_code(500);
    exit;
}
header('Content-Type: image/png');

while (($reading = fgets($handle, 4096)) !== false) {
    echo $reading;
}

fclose($handle);
