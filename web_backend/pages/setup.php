<?php

/**
 * @var \NetzwerkMedienObrs\Sqlite Connect to the database
 */
$pdo = new \NetzwerkMedienObrs\Sqlite;
$pdo->connect();
$lastMail = null;
$lastSession = $pdo->getCurrentSession();
if (is_object($lastSession) && isset($lastSession->userid)) {
    $lastMail = $lastSession->userid;
}
$pdo->endCurrentSession();
$pdo->endSessions();
$pdo->insertSession($email);

/**
 * Send welcome mail
 */
if (strcmp($lastMail, $email) !== 0 && (!defined("OBRS_LOGIN_HANDLER_INTERNAL") || OBRS_LOGIN_HANDLER != OBRS_LOGIN_HANDLER_INTERNAL)) {
    \NetzwerkMedienObrs\sendVideoLink($email);
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>
        <?php echo $strings->getString('NAME'); ?>
    </title>
    <?php
    require_once(OBRS_BASE_PATH . "/tpl/pwa/tags.php");
    ?>
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_COMMON; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_SETUP; ?>">
    <script src="<?php echo OBRS_WEB_JS_COMMON; ?>"></script>
    <script src="<?php echo OBRS_WEB_JS_SCREENSAVER; ?>"></script>
</head>

<body>

    <div id="menu">
        <div class="button-single button-single-menu button-single-left" id="reset" onclick="resetSetup()">
            <?php echo $stringsGeneral->getString('RESET'); ?>
        </div>
        <div class="button-single button-single-menu button-single-right" id="logout"><a href="./?logout=1">
                <?php echo $stringsGeneral->getString('LOGOUT'); ?>
            </a></div>
    </div>

    <div id="main-container">
        <div class="slides hidden fade" data-progress="0" id="background">
            <h1>
                <?php echo $strings->getString('BACKGROUND_NAME'); ?>
            </h1>
            <div class="button" onclick="setBackground(207)">
                <img src="../img/private_branding/icon_bg_default.png" />
                <div>
                    <?php echo $strings->getString('BACKGROUND_DEFAULT'); ?>
                </div>
            </div>
            <?php
            if (defined("OBRS_DEPARTMENT_BACKGROUND_COUNT") && OBRS_DEPARTMENT_BACKGROUND_COUNT > 0) {
            ?>
                <div class="button" onclick="currentSlide('background2')">
                    <img src="../img/private_branding/icon_bg_department.png" />
                    <div>
                        <?php echo $strings->getString('BACKGROUND_DEPARTMENT'); ?>
                    </div>
                </div>
            <?php } ?>
            <div id="bg_button_user" class="hidden button" onclick="currentSlide('background3')">
                <img src="../img/setup/icon_bg_user.png" />
                <div>
                    <?php echo $strings->getString('BACKGROUND_OWN'); ?>
                </div>
            </div>
            <?php if (defined("OBRS_SUPPORT_GREEN_SCREEN") && OBRS_SUPPORT_GREEN_SCREEN) { ?>
                <div class="button" onclick="setBackground(-2)">
                    <img src="../img/setup/icon_bg_green_screen.png" />
                    <div>
                        <?php echo $strings->getString('BACKGROUND_GREEN_SCREEN'); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="slides hidden fade" data-progress="25" id="background2">
            <h1>
                <?php echo $strings->getString('BACKGROUND_NAME'); ?>
            </h1>
            <h2>
                <?php echo $strings->getString('BACKGROUND_SELECT_DESC_DEPARTMENT'); ?>
            </h2>
            <?php
            if (defined("OBRS_DEPARTMENT_BACKGROUND_COUNT")) {
                for ($i = 0; $i < OBRS_DEPARTMENT_BACKGROUND_COUNT && $i < 99; $i++) {
                    echo '<div class="';
                    if (OBRS_DEPARTMENT_BACKGROUND_COUNT > 8) {
                        echo 'small';
                    }
                    echo ' button middle" onclick="setBackground(' . ($i + 101) . ')"><img src="../img/private_branding/' . ($i + 101) . '.png" alt=""/></div>';
                }
            }
            ?>
        </div>
        <div class="slides hidden fade" data-progress="25" id="background3">
            <h1>
                <?php echo $strings->getString('BACKGROUND_NAME'); ?>
            </h1>
            <h2>
                <?php echo $strings->getString('BACKGROUND_SELECT_DESC_OWN'); ?>
            </h2>
            <div id="background3inner"></div>
        </div>
        <div class="slides hidden fade" data-progress="50" id="media">
            <h1>
                <?php echo $strings->getString('SOURCES_NAME'); ?>
            </h1>
            <?php if (defined("OBRS_DOCUMENT_CAM") && OBRS_DOCUMENT_CAM) { ?>
                <div class="button" id="doccamoption" onclick="setMedia(1)"><img src="../img/setup/icon_media_doccam.png" />
                    <div>
                        <?php echo $strings->getString('SOURCES_DOCCAM'); ?>
                    </div>
                </div>
            <?php } ?>
            <div class="button" onclick="setMedia(2)"><img src="../img/setup/icon_media_presentation.png" />
                <div>
                    <?php echo $strings->getString('SOURCES_PRESENTATION'); ?>
                </div>
            </div>
            <div class="button" onclick="setMedia(0)"><img src="../img/setup/icon_media_none.png" />
                <div>
                    <?php echo $strings->getString('SOURCES_NONE'); ?>
                </div>
            </div>
        </div>
        <div class="slides hidden fade" data-progress="100" id="final">
            <?php
            if (defined("OBRS_SINGLE_IPAD") && OBRS_SINGLE_IPAD) {
            ?>
                <div id="room-link"><a href="../room">
                        <?php echo $strings->getString('ROOM_LINK'); ?>
                    </a></div>
            <?php } else { ?>
                <h1>
                    <?php echo $strings->getString('ROOM_TITLE'); ?>
                </h1>
                <img src="../img/setup/room.png" height="200" />
                <h2>
                    <?php echo $strings->getString('ROOM_DESC'); ?>
                </h2>
                <h3>
                    <?php echo $strings->getString('ROOM_DESC_SUB'); ?>
                </h3>
            <?php } ?>
        </div>
    </div>

    <div class="overlayContainer" id="errorMsg">
        <div class="overlayContainerInner">
            <div id="errorMsgInner">
                <?php echo $stringsGeneral->getString('ERROR'); ?>
            </div>
            <div><button onclick="window.location.reload();">
                    <?php echo $stringsGeneral->getString('ERROR_DISMISS'); ?>
                </button></div>
        </div>
    </div>

    <div id="progress"></div>

    <script src=" <?php echo OBRS_WEB_JS_SETUP; ?>"></script>

</body>

</html>
