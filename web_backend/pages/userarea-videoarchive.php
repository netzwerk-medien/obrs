<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php echo $stringsUserArea->getString('AREA_VIDEOARCHIVE'); ?>
    </title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_VIDEOARCHIVE; ?>">
    <script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
    <?php
    /**
     * Videoarchive
     */

    $stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
    if ($stringsGeneral->hasErrors()) {
        echo OBRS_I18N_ERROR;
        throw new \Exception($stringsGeneral->listErrors());
    }
    $stringsUserArea = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_USER_AREA);
    if ($stringsUserArea->hasErrors()) {
        echo OBRS_I18N_ERROR;
        throw new \Exception($stringsUserArea->listErrors());
    }

    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();
    $logins = $pdo->getLogins($email);
    if ($handle = opendir(OBRS_SHELL_MEDIA_PATH_RECORDS)) {
        while (false !== ($file = readdir($handle))) {
            if (is_file(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$file")) {
                foreach ($logins as $lastLogin) {
                    if (!empty($lastLogin["sessiontimeend"]) && filemtime(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$file") > strtotime($lastLogin["sessiontime"]) + date("Z") && filemtime(OBRS_SHELL_MEDIA_PATH_RECORDS . "/$file") < strtotime($lastLogin["sessiontimeend"]) + date("Z") && !$pdo->doesVideoExist($file)) {
                        $pdo->insertVideo($file, (object) $lastLogin);
                        if (defined("OBRS_LOG_PATH")) {
                            $videoMissingLogFile = OBRS_LOG_PATH . "/videomissing.log";
                            if (!file_exists($videoMissingLogFile)) {
                                touch($videoMissingLogFile);
                            }
                            if (file_exists($videoMissingLogFile) && is_file($videoMissingLogFile)) {
                                file_put_contents($videoMissingLogFile, date("Y-m-d H:i:s") . ": " . $file . PHP_EOL, FILE_APPEND);
                            }
                        }
                    }
                }
            }
        }
        closedir($handle);
    }

    $nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
    $nav->getHTML();

    /**
     * Print videos belonging to user
     */
    $arr = $pdo->getVideoList($email);
    echo '<div id="videoarchive_list">';
    if (count($arr) > 0) {
        foreach ($arr as $video) {
            echo '<div class="videoarchive_item">';
            echo '<video controls playsinline preload="none" poster="/thumbnails/' . substr($video['filename'], 0, -4) . '.jpg"><source class="videosource" src="/video/' . $video['filename'] . '" type="' . OBRS_VIDEO_FILE_MIME . '"></video><br>';
            echo '<div style="display:block;margin:20px;"><a href="/video/' . $video['filename'] . '">' . $video['filename'] . '</a><br><a download href="/video/' . $video['filename'] . '">' . $stringsUserArea->getString("VIDEOARCHIVE_DOWNLOAD") . '</a></div>';
            echo '</div>';
        }
    } else {
        echo $stringsUserArea->getString("VIDEOARCHIVE_NONE");
    }
    echo '</div>';
    ?>
</body>

</html>
