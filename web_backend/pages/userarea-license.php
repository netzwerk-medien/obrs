<!doctype html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $stringsUserArea->getString('AREA_LICENSE'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
	<script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
	<?php
	$nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
	$nav->getHTML();
	?>
	<main>
		<article id="about">
			<header>
				<h1><?php echo $stringsUserArea->getString('AREA_LICENSE'); ?></h1>
			</header>
			<div class="article-main">
				<p><?php echo $stringsUserArea->getString('LICENSE_ABOUT'); ?></p>
			</div>
		</article>
		<article id="icon">
			<header>
				<h2><?php echo $stringsUserArea->getString('LICENSE_ICON_NAME'); ?></h2>
			</header>
			<div class="article-main">
				<p><?php echo $stringsUserArea->getString('LICENSE_ICON_INTRO', '<a href="https://material.io/resources/icons/">' . $stringsUserArea->getString('LICENSE_ICON_NAME') . '</a>'); ?></p>
				<p><?php echo $stringsUserArea->getString('LICENSE_ICON_LEGAL', '<a href="/fonts/MaterialIcons/license.txt">' . $stringsUserArea->getString('LICENSE_ICON_LICENSE') . '</a>'); ?></p>
			</div>
		</article>
		<article id="font">
			<header>
				<h2><?php echo $stringsUserArea->getString('LICENSE_FONT_NAME'); ?></h2>
			</header>
			<div class="article-main">
				<p><?php echo $stringsUserArea->getString('LICENSE_FONT_INTRO', '<a href="https://www.google.com/get/noto/#sans-lgc">' . $stringsUserArea->getString('LICENSE_FONT_NAME') . '</a>'); ?></p>
				<p><?php echo $stringsUserArea->getString('LICENSE_FONT_LEGAL', '<a href="/fonts/NotoSans/LICENSE_OFL.txt">' . $stringsUserArea->getString('LICENSE_FONT_LICENSE') . '</a>'); ?></p>
			</div>
		</article>
	</main>
</body>

</html>
