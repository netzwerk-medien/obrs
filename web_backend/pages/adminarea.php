<?php

/**
 * Admin area backend
 */

?>
<!doctype html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $stringsAdminArea->getString('NAME'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
	<style>
		#usage {
			text-align: center;
		}

		#welcome a {
			color: inherit;
			text-decoration: none;
		}
	</style>
	<script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
	<?php
	$nav = new \NetzwerkMedienObrs\AdminAreaNavigation($stringsAdminArea);
	$nav->getHTML();
	?>
	<main>
		<header id="usage">
			<h1>
				<?php
				$pdo = new \NetzwerkMedienObrs\Sqlite;
				$pdo->connect();
				echo ($pdo->getCurrentSession())->session_active ? $stringsAdminArea->getString('HOME_STUDIO_ACTIVE') :  $stringsAdminArea->getString('HOME_STUDIO_INACTIVE');
				?>
			</h1>
		</header>
		<article id="welcome">
			<header>
				<h2><?php echo $stringsAdminArea->getString('HOME_WELCOME', $email); ?></h2>
			</header>
			<div class="article-main">
				<p><a href="<?php echo OBRS_LINK_ADMIN_AREA; ?>/videomgr/"><?php echo $stringsAdminArea->getString('AREA_VIDEOMANAGER'); ?></a></p>
				<p><a href="<?php echo OBRS_LINK_ADMIN_AREA; ?>/addadmin/"><?php echo $stringsAdminArea->getString('AREA_ADDADMIN'); ?></a></p>
				<p><a href="<?php echo OBRS_LINK_ADMIN_AREA; ?>/viewlogs/"><?php echo $stringsAdminArea->getString('AREA_VIEW_LOGS'); ?></a></p>
				<p><a href="<?php echo OBRS_LINK_ADMIN_AREA; ?>/controlstudio/"><?php echo $stringsAdminArea->getString('AREA_CONTROL_STUDIO'); ?></a></p>
				<p><a href="<?php echo OBRS_LINK_ADMIN_AREA; ?>/db/"><?php echo $stringsAdminArea->getString('AREA_DB'); ?></a></p>
			</div>
		</article>
	</main>
</body>

</html>
