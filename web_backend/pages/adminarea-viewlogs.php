<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $stringsAdminArea->getString('AREA_VIEW_LOGS'); ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
    <style>
        .comfortable {
            margin: 10px;
            padding: 5px;
        }

        .table-comfortable {
            border-collapse: separate;
            border-spacing: 15px;
        }
    </style>
    <script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
    <?php
    /**
     * View Logs
     */

    $nav = new \NetzwerkMedienObrs\AdminAreaNavigation($stringsAdminArea);
    $nav->getHTML();

    if (isset($_GET["log"]) && !empty($_GET["log"])) {
        $log = preg_replace("#/#", "", urldecode($_GET["log"]));
        if (file_exists(OBRS_LOG_PATH . "/$log") && is_file(OBRS_LOG_PATH . "/$log")) {
            echo preg_replace("/\n/", "<br>", file_get_contents(OBRS_LOG_PATH . "/$log"));
        }
    } else {
        $files = array();
        if ($handle = opendir(OBRS_LOG_PATH)) {
            while (false !== ($file = readdir($handle))) {
                if (is_file(OBRS_LOG_PATH . "/$file")) {
                    array_push($files, $file);
                }
            }
            closedir($handle);
        }
        sort($files);
        echo "<table class=\"table-comfortable\"><tr><th>" . $stringsAdminArea->getString('VIEW_LOGS_LOG') . "</th></tr>";
        foreach ($files as $file) {
            echo "<tr><td><a href=\"?log=" . urlencode($file) . "\">$file</a></td></tr>";
        }
        echo "</table>";
    }
    ?>
</body>

</html>
