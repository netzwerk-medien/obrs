<!doctype html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $stringsUserArea->getString('AREA_PICS'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
	<script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
	<style>
		#upload form {
			background: #204356;
			color: #ddd;
		}

		#upload input {
			background: #ddd;
			color: #001;
			border: none;
			border-radius: 6px;
			padding: 6px;
			margin: 12px;
			font-size: 1.01em;
		}
	</style>
</head>

<body>
	<?php
	$nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
	$nav->getHTML();
	?>
	<main>
		<article id="start">
			<header>
				<h1><?php echo $stringsUserArea->getString('AREA_PICS'); ?></h1>
			</header>
			<p><?php echo $stringsUserAreaPics->getString('HOME_START_INTRO'); ?></p>
		</article>
		<article id="infos">
			<header>
				<h2><?php echo $stringsUserAreaPics->getString('HOME_INFOS_NAME'); ?></h2>
			</header>
			<div class="article-main">
				<p><?php echo $stringsUserAreaPics->getString('HOME_INFOS_INTRO', array(OBRS_USER_AREA_PICS_WIDTH, OBRS_USER_AREA_PICS_HEIGHT, OBRS_USER_AREA_PICS_MAX_SIZE)); ?></p>
				<p><?php echo $stringsUserAreaPics->getString('HOME_INFOS_ERROR', array(OBRS_USER_AREA_PICS_WIDTH, OBRS_USER_AREA_PICS_HEIGHT, OBRS_USER_AREA_PICS_MAX_NUMBER, OBRS_USER_AREA_PICS_RATIO)); ?></p>
			</div>
		</article>
		<article id="upload">
			<header>
				<h2><?php echo $stringsUserAreaPics->getString('HOME_UPLOAD_NAME'); ?></h2>
			</header>
			<form class="article-main" accept-charset="UTF-8" action="bilder.php" method="post" enctype="multipart/form-data">
				<input type="file" multiple="multiple" required accept="image/*" name="bilder[]">
				<input value="<?php echo $stringsUserAreaPics->getString('HOME_UPLOAD_SUBMIT'); ?>" type="submit">
			</form>
		</article>
		<article id="manage">
			<header>
				<h2><?php echo $stringsUserAreaPics->getString('MANAGE_NAME'); ?></h2>
			</header>
			<div class="article-main">
				<p><?php echo $stringsUserAreaPics->getString('HOME_MANAGE_DESCRIPTION', '<a href="manage">' . $stringsUserAreaPics->getString('HOME_MANAGE_DESCRIPTION_LINK') . '</a>'); ?></p>
			</div>
		</article>
		<article id="crop">
			<header>
				<h2><?php echo $stringsUserAreaPics->getString('CROP_NAME'); ?></h2>
			</header>
			<div class="article-main">
				<p><?php echo $stringsUserAreaPics->getString('HOME_CROP_DESCRIPTION', '<a href="crop">' . $stringsUserAreaPics->getString('HOME_CROP_DESCRIPTION_LINK') . '</a>'); ?></p>
			</div>
		</article>
	</main>
</body>

</html>
