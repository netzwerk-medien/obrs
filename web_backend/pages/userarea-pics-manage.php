<?php
$pdo = new \NetzwerkMedienObrs\Sqlite;
$pdo->connect();
?>
<!doctype html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $stringsUserAreaPics->getString('MANAGE_NAME'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA_PICS_GRID; ?>">
	<script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>

	<style>
		.pics-dialog-wrapper {
			position: fixed;
			width: 100vw;
			height: 100vh;
			border: 0px;
			padding: 0px;
			margin: 0px;
			top: 0;
			left: 0;
			z-index: 20000;
			display: flex;
			flex-direction: column;
			align-content: center;
			align-items: center;
			background: rgba(0, 0, 0, 0.5);
		}

		.pics-dialog {
			position: relative;
			width: 75%;
			border: 4px solid black;
			padding: 14px;
			margin: 0px;
			top: 100px;
			background: #7fafbf;
		}

		.pics-dialog-text {
			font-size: 1.5em;
		}

		.pics-dialog-buttons {
			display: flex;
			flex-direction: row;
			justify-content: center;
			justify-items: center;
			align-content: center;
			align-items: center;
		}
	</style>
</head>

<body>
	<?php
	$nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
	$nav->getHTML();
	?>
	<main>
		<article id="pics">
			<header>
				<h1><?php echo $stringsUserAreaPics->getString('MANAGE_NAME'); ?></h1>
			</header>
			<div class="article-main">
				<?php
				$pics = $pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_FINAL);
				if (count($pics) == 0) {
				?>
					<div id="pics-warn"><?php echo $stringsUserAreaPics->getString('MANAGE_NONE'); ?></div>
				<?php
				} else {
				?>
					<div id="pics-inner">
						<?php
						foreach ($pics as $pic) {
							echo '
									<div class="pics-inner-item">
											<img src="' . OBRS_WEB_PATH_BACKGROUNDS_USER_FINAL . '/' . $pic['filename'] . '" alt="' . $stringsUserAreaPics->getString('MANAGE_ALT') . '">
											<form class="pic-inner-form" accept-charset="UTF-8" action="bilder.php" method="post">
												<input type="hidden" value="delete" name="mode">
												<input type="hidden" value="' . $pic['filename'] . '" name="delete">
											</form>
											<button class="pic-inner-submit">' . $stringsUserAreaPics->getString('MANAGE_DELETE') . '</button>
									</div>
								';
						}
						?>
					</div>
					<div id="pics-dialog-delete" class="pics-dialog-wrapper hidden">
						<div class="pics-dialog">
							<div class="pics-dialog-text">
								<?php echo $stringsUserAreaPics->getString('MANAGE_DELETE_DIALOG'); ?>
							</div>
							<div class="pics-dialog-buttons">
								<button class="pics-dialog-submit"><?php echo $stringsUserAreaPics->getString('MANAGE_DELETE_DIALOG_CONFIRM'); ?></button>
								<button class="pics-dialog-cancel"><?php echo $stringsUserAreaPics->getString('MANAGE_DELETE_DIALOG_CANCEL'); ?></button>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</article>
	</main>
	<script>
		document.querySelectorAll('.pic-inner-submit').forEach(function(elem) {
			elem.addEventListener('click', function(e) {
				var deleteConfirm = document.querySelector('#pics-dialog-delete');
				deleteConfirm.classList.remove('hidden');
				deleteConfirm.querySelector('.pics-dialog-cancel').onclick = function() {
					deleteConfirm.classList.add('hidden');
				};
				deleteConfirm.querySelector('.pics-dialog-submit').onclick = function() {
					deleteConfirm.classList.add('hidden');
					elem.parentNode.querySelector('.pic-inner-form').submit();
				};
			});
		});
	</script>
</body>

</html>
