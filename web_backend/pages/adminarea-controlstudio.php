<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php echo $stringsAdminArea->getString('AREA_CONTROL_STUDIO'); ?>
    </title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
    <style>
        #warning {
            color: red;
        }

        div.input {
            display: inline-block;
        }

        div.input>button {
            border: none;
        }

        .rr {
            display: inline-block;
            width: 70px;
            height: 70px;
            line-height: 70px;
            background-color: #000;
            border-radius: 20px;
            text-decoration: none;
            color: white;
            cursor: pointer;
            font-weight: bold;
            font-size: 15px;
            text-align: center;
            background: linear-gradient(to top right, rgba(63, 81, 181, 1) 0%,
                    rgba(3, 155, 229, 1) 40%);
            box-shadow: 1px 1px 30px rgba(2, 119, 189, 0.5);
            margin: 10px;
        }

        .rr:hover {
            box-shadow: 1px 1px 30px rgba(2, 119, 189, 1);
        }

        .rrdark {
            background: linear-gradient(to top right, rgba(22, 44, 66, 1) 0%,
                    rgba(3, 44, 33, 1) 40%);
            box-shadow: 1px 1px 30px rgba(2, 2, 2, 0.5);
        }

        .rrdark:hover {
            box-shadow: 1px 1px 30px rgba(2, 2, 2, 1);
        }

        .rrgreen {
            background: linear-gradient(to top right, rgba(22, 222, 66, 1) 0%,
                    rgba(3, 177, 33, 1) 40%);
            box-shadow: 1px 1px 30px rgba(119, 189, 2, 0.5);
        }

        .rrgreen:hover {
            box-shadow: 1px 1px 30px rgba(119, 189, 2, 1);
        }

        .rrred {
            background: linear-gradient(to top right, rgba(222, 22, 66, 1) 0%,
                    rgba(177, 3, 33, 1) 40%);
            box-shadow: 1px 1px 30px rgba(189, 119, 2, 0.5);
        }

        .rrred:hover {
            box-shadow: 1px 1px 30px rgba(189, 119, 2, 1);
        }
    </style>
    <script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
    <?php
    /**
     * Studio control
     */

    $nav = new \NetzwerkMedienObrs\AdminAreaNavigation($stringsAdminArea);
    $nav->getHTML();
    echo '<header id="warning"><h1>' . $stringsAdminArea->getString('CONTROL_STUDIO_WARN') . '</h1></header>';

    $scenesLocation = OBRS_SHELL_INTERFACE_PATH_SCENES;
    $query = htmlspecialchars($_SERVER['QUERY_STRING']);

    $action = null;
    $key = null;
    $bg = null;
    if (isset($_POST['action'])) {
        $action = intval($_POST['action']);
    } else if (isset($_POST['key'])) {
        $key = in_array($_POST['key'], OBRS_PROFILE_KEYS) ? strval($_POST['key']) : null;
    } else if (isset($_POST['bg'])) {
        $bg = intval($_POST['bg']);
    }

    if ($action === null && $key === null && $bg === null) {
        echo '<div class="input"><input name="action" value="0" type="hidden"><button>ENTER</button></div>';
    } else {
        if ($key !== null) {
            if (defined("OBRS_NO_LIGHTS") && OBRS_NO_LIGHTS) {
                $dmx = -1;
            } else if (defined("OBRS_DMX_VALUES")) {
                $dmx = OBRS_DMX_VALUES;
            } else {
                $dmx = "255";
            }
            $command = sprintf("DISPLAY=:0 $scenesLocation/switchscenario.sh -d %s -k %s", $dmx, $key);
            exec($command, $returnText, $returnSuccess);
            echo "script output: " . implode(", ", $returnText) . '<br>';
            echo "script exit status: " . $returnSuccess . '<br>';
        } else if ($bg !== null) {
            $command = sprintf("$scenesLocation/switchbackground.sh -b %s -u 0", $bg);
            exec($command, $returnText, $returnSuccess);
            echo "script output: " . implode(", ", $returnText) . '<br>';
            echo "script exit status: " . $returnSuccess . '<br>';
        } else {
            switch ($action) {
                    # start recording
                case 9:
                    $command = "DISPLAY=:0 $scenesLocation/recordstart.sh";
                    exec($command, $returnText, $returnSuccess);
                    echo "script output: " . implode(", ", $returnText) . '<br>';
                    echo "script exit status: " . $returnSuccess . '<br>';
                    break;

                    # stop recording
                case 10:
                    $command = "DISPLAY=:0 $scenesLocation/recordstop.sh";
                    exec($command, $returnText, $returnSuccess);
                    echo "script output: " . implode(", ", $returnText) . '<br>';
                    echo "script exit status: " . $returnSuccess . '<br>';
                    break;

                    # reset studio environment and go back to main page
                case 11:
                    $command = "DISPLAY=:0 $scenesLocation/clean.sh";
                    exec($command, $returnText, $returnSuccess);
                    echo "script output: " . implode(", ", $returnText) . '<br>';
                    echo "script exit status: " . $returnSuccess . '<br>';
                    break;

                    # reboot studio
                case 12:
                    $command = "sudo /usr/sbin/reboot";
                    exec($command, $returnText, $returnSuccess);
                    echo "script output: " . implode(", ", $returnText) . '<br>';
                    echo "script exit status: " . $returnSuccess . '<br>';
                    break;
            }
        }
        $video = \NetzwerkMedienObrs\getLatestVideo();
        $stats = stat(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $video);
        echo '<br>Latest Video on file system: ' . $video . ', size: ' . round($stats[7] / (1024 * 1024), 1) . ' MB, last modified: ' . date('Y-m-d H:i:s', $stats[9]) . '.';
        if ($action != 9) {
            echo '<h3 id="action">recordstart.sh / recordstop.sh / clean.sh</h3><div class="input"><input name="action" value="9" type="hidden"><button class="rr rrgreen">START</a></div>';
        } else {
            echo '<h3 id="action">recordstop.sh</h3>';
        }
        echo '<div class="input"><input name="action" value="10" type="hidden"><button class="rr rrred">STOP</button></div>';
        if ($action != 9) {
            echo '<div class="input"><input name="action" value="11" type="hidden"><button class="rr rrdark">CLEAN</button></div>';
            echo '<h3 id="key">switchscenario.sh</h3>';
            foreach (OBRS_PROFILE_KEYS as $profile => $key) {
                echo '<div class="input"><input name="key" value="' . $key . '" type="hidden"><button class="rr">' . $profile . '</button></div>';
            }
            echo '<h3 id="bg">switchbackground.sh</h3>';
            for ($i = 0; $i < OBRS_DEPARTMENT_BACKGROUND_COUNT; $i++) {
                echo '<div class="input"><input name="bg" value="' . ($i + 101) . '" type="hidden"><button class="rr">' . ($i + 101) . '</button></div>';
            }
            echo '<div class="input"><input name="bg" value="207" type="hidden"><button class="rr">neutral</button></div>';
            echo '<div class="input"><input name="bg" value="300" type="hidden"><button class="rr">green</button></div>';
            echo '<h3 id="reboot">reboot</h3>';
            echo '<div class="input"><input name="action" value="12" type="hidden"><button class="rr rrdark">REBOOT</button></div>';
        }
    }
    ?>
    <form action="#" method="post"></form>
    <script>
        var buttons = document.querySelectorAll("div.input > button");
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener("click", function(event) {
                input = event.target.parentNode.querySelector("input");
                var form = document.querySelector("form");
                form.appendChild(input);
                form.action = "#" + input.name;
                form.submit();
            });
        }
    </script>
</body>

</html>
