<?php

/**
 * Thumbnail
 */

/**
 * Does file exist
 */
if (!file_exists(OBRS_SHELL_MEDIA_PATH_THUMBS . "/$thumbnail")) {
    http_response_code(404);
    exit;
}

/**
 * Read image
 */
$handle = fopen(OBRS_SHELL_MEDIA_PATH_THUMBS . "/$thumbnail", 'r');
if ($handle === false) {
    http_response_code(500);
    exit;
}
header('Content-Type: image/jpeg');

while (($reading = fgets($handle, 4096)) !== false) {
    echo $reading;
}

fclose($handle);
