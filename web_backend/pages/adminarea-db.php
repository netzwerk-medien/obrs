<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php echo $stringsAdminArea->getString('AREA_DB'); ?>
    </title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
    <style>
        article {
            padding-top: 100px;
        }

        article:target>div {
            background: rgba(255, 0, 0, 0.1);
            width: 100%;
        }

        #jumptocontent {
            position: fixed;
            background: inherit;
        }
    </style>
    <script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
    <?php
    /**
     * DB view
     */

    $nav = new \NetzwerkMedienObrs\AdminAreaNavigation($stringsAdminArea);
    $nav->getHTML();

    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();

    $tables = $pdo->getTables();
    echo '<nav id="jumptocontent">' . $stringsAdminArea->getString('DB_TABLES');
    foreach ($tables as $table) {
        echo " | <a href=\"#$table\">$table</a>";
    }
    echo '</nav>';

    echo '<article id="sessions"><h3>sessions</h3><div>';
    $sessions = $pdo->getSessions();
    if (count($sessions) > 0) {
        echo "<div>" . implode(", ", array_keys($sessions[0])) . "</div>";
        foreach ($sessions as $session) {
            echo "<div>" . implode(", ", $session) . "</div>";
        }
    }
    echo '</div></article>';

    echo '<article id="videos"><h3>videos</h3><div>';
    $videos = $pdo->getAllVideos();
    if (count($videos) > 0) {
        echo "<div>" . implode(", ", array_keys($videos[0])) . "</div>";
        foreach ($videos as $video) {
            echo "<div>" . implode(", ", $video) . "</div>";
        }
    }
    echo '</div></article>';

    echo '<article id="backgrounds"><h3>backgrounds</h3><div>';
    $backgrounds = $pdo->getAllBackgrounds();
    if (count($backgrounds) > 0) {
        echo "<div>" . implode(", ", array_keys($backgrounds[0])) . "</div>";
        foreach ($backgrounds as $background) {
            echo "<div>" . implode(", ", $background) . "</div>";
        }
    }
    echo '</div></article>';

    echo '<article id="users"><h3>users (OBRS_LOGIN_HANDLER_INTERNAL)</h3><div>';
    $users = $pdo->getUsers();
    if (count($users) > 0) {
        echo "<div>" . implode(", ", array_keys($users[0])) . "</div>";
        foreach ($users as $user) {
            echo "<div>" . implode(", ", $user) . "</div>";
        }
    }
    echo '</div></article>';

    echo '<article id="admins"><h3>admins</h3><div>';
    if (defined("OBRS_SUPER_ADMIN")) {
        echo "OBRS_SUPER_ADMIN: " . OBRS_SUPER_ADMIN;
    }
    $admins = $pdo->getAdmins();
    if (count($admins) > 0) {
        echo "<div>" . implode(", ", array_keys($admins[0])) . "</div>";
        foreach ($admins as $admin) {
            echo "<div>" . implode(", ", $admin) . "</div>";
        }
    }
    echo '</div></article>';
    ?>
</body>

</html>
