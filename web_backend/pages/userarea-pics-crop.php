<?php
$pdo = new \NetzwerkMedienObrs\Sqlite;
$pdo->connect();
?>
<!doctype html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $stringsUserAreaPics->getString('CROP_NAME'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA_PICS_GRID; ?>">
	<script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
	<?php
	$nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
	$nav->getHTML();
	?>
	<main>
		<article id="pics">
			<header>
				<h1><?php echo $stringsUserAreaPics->getString('CROP_NAME'); ?></h1>
			</header>
			<div class="article-main">
				<?php
				$pics = $pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_CROP);
				if (count($pics) == 0) {
				?>
					<div id="pics-ok"><?php echo $stringsUserAreaPics->getString('CROP_NONE'); ?></div>
				<?php
				} else {
				?>
					<div id="pics-warn"><?php echo $stringsUserAreaPics->getString('CROP_WARN'); ?></div>
					<div id="pics-inner">
						<?php
						foreach ($pics as $pic) {
							echo '
									<div class="pics-inner-item">
											<img src="' . OBRS_WEB_PATH_BACKGROUNDS_USER_CROP . '/' . $pic['filename'] . '" alt="' . $stringsUserAreaPics->getString('CROP_ALT') . '">
											<form accept-charset="UTF-8" action="crop.php" method="post">
												<input type="hidden" value="' . $pic['filename'] . '" name="pic">
												<input value="' . $stringsUserAreaPics->getString('CROP_BUTTON') . '" type="submit">
											</form>
									</div>
								';
						}
						?>
					</div>
				<?php } ?>
			</div>
		</article>
	</main>
</body>

</html>
