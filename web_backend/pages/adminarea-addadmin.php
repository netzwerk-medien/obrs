<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $stringsAdminArea->getString('AREA_ADDADMIN'); ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
    <style>
        .list-item {
            padding-bottom: 20px;
        }
    </style>
    <script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
    <?php
    /**
     * Add admin
     */

    $nav = new \NetzwerkMedienObrs\AdminAreaNavigation($stringsAdminArea);
    $nav->getHTML();

    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();
    if (isset($_GET["newname"]) && isset($_GET["newlevel"]) && !empty($_GET["newname"]) && !empty($_GET["newlevel"]) && !empty($email)) {
        $newname = preg_replace("#['\";]#", "", urldecode($_GET["newname"]));
        $newlevel = intval($_GET["newlevel"]);
        $ownSuperAdmin = \netzwerkMedienObrs\checkSuperAdmin($email);
        $ownlevel = $ownSuperAdmin ? -1 : $pdo->adminAuthLevel($email);
        if ($newlevel > 0 && !empty($newname) && !$pdo->isAdmin($newname) && ($ownSuperAdmin || ($ownlevel >= $newlevel))) {
            $success = $pdo->createAdmin($newname, $newlevel);
            $result = $success ? $stringsAdminArea->getString("ADDADMIN_OKAY") : $stringsAdminArea->getString("ADDADMIN_ERROR");
            echo $result . '<br><a href="./?">' . $stringsAdminArea->getString("ADDADMIN_BACK") . '</a>';
            if (defined("OBRS_LOG_PATH")) {
                $logfile = OBRS_LOG_PATH . "/admin_add_admin.log";
                if (!file_exists($logfile)) {
                    touch($logfile);
                }
                if (file_exists($logfile) && is_file($logfile)) {
                    file_put_contents($logfile, date("Y-m-d H:i:s") . ": Add $newname with level $newlevel by $email with level $ownlevel. Result: $result" . PHP_EOL, FILE_APPEND);
                }
            }
        } else {
            echo $stringsAdminArea->getString('ADDADMIN_ERROR_LEVEL');
        }
    } else if (isset($_GET["name"]) && isset($_GET["changelevel"]) && !empty($_GET["name"]) && !empty($email)) {
        $name = preg_replace("#['\";]#", "", urldecode($_GET["name"]));
        $level = intval($_GET["changelevel"]);
        $oldlevel = $pdo->adminAuthLevel($name);
        $ownSuperAdmin = \netzwerkMedienObrs\checkSuperAdmin($email);
        $ownlevel = $ownSuperAdmin ? -1 : $pdo->adminAuthLevel($email);
        if ($level >= 0 && !empty($name) && $pdo->isAdmin($name) && ($ownSuperAdmin || ($ownlevel > $oldlevel && $ownlevel >= $level))) {
            $success = $pdo->changeAdminLevel($name, $level);
            $result = $success ? $stringsAdminArea->getString("ADDADMIN_OKAY") : $stringsAdminArea->getString("ADDADMIN_ERROR");
            echo $result . '<br><a href="./?">' . $stringsAdminArea->getString("ADDADMIN_BACK") . '</a>';
            if (defined("OBRS_LOG_PATH")) {
                $logfile = OBRS_LOG_PATH . "/admin_change_admin.log";
                if (!file_exists($logfile)) {
                    touch($logfile);
                }
                if (file_exists($logfile) && is_file($logfile)) {
                    file_put_contents($logfile, date("Y-m-d H:i:s") . ": Change $name with level $oldlevel >> $level by $email with level $ownlevel. Result: $result" . PHP_EOL, FILE_APPEND);
                }
            }
        } else {
            echo $stringsAdminArea->getString('ADDADMIN_ERROR_LEVEL');
        }
    } else {
        echo "<div><h2>" . $stringsAdminArea->getString('ADDADMIN_NEW') . '</h2><div>' . $stringsAdminArea->getString('ADDADMIN_NEW_DESC') . '</div><form>' . $stringsAdminArea->getString('ADDADMIN_NAME') . ' <input type="text" name="newname"><br>' . $stringsAdminArea->getString('ADDADMIN_LEVEL') . ' <input type="number" name="newlevel"><br><input type="submit"></form></div>';
        echo "<div><h2>" . $stringsAdminArea->getString('ADDADMIN_CHANGE') . '</h2><div>' . $stringsAdminArea->getString('ADDADMIN_CHANGE_DESC') . '</div>';
        foreach ($pdo->getAdmins() as $admin) {
            echo '<div class="list-item"><form>' . $stringsAdminArea->getString('ADDADMIN_NAME') . ' <input readonly type="text" value="' . $admin['userid'] . '" name="name"><br>' . $stringsAdminArea->getString('ADDADMIN_LEVEL') . ' <input type="number" value="' . $admin['auth_level'] . '" name="changelevel"><br><input type="submit"></form></div>';
        }
        echo "</div>";
    }
    ?>
</body>

</html>
