<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>
        <?php echo $strings->getString('NAME'); ?>
    </title>
    <?php require_once(OBRS_BASE_PATH . "/tpl/pwa/tags.php") ?>
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_COMMON; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_ROOM; ?>">
    <script src="<?php echo OBRS_WEB_JS_COMMON; ?>"></script>
    <script src="<?php echo OBRS_WEB_JS_SCREENSAVER; ?>"></script>
</head>

<body>

    <div id="main">
        <div id="record">
            <div id="main-container">
                <div class="fade round-button" id="record-button" onclick="startRecordingAfterCountDown()">
                    <h1 id="record-button-inner-text">
                        <?php echo $strings->getString('START_RECORDING'); ?>
                    </h1>
                </div>

                <div id="countdown-display"></div>

                <div class="fade round-button" id="stop-button" onclick="stopRecording()">
                    <h1 id="time-display">00:00:00</h1>
                </div>

                <div class="fade round-button" id="activityindicator">
                    <img src="../img/video/activityindicator.gif">
                </div>
            </div>

            <?php if (defined("OBRS_SINGLE_IPAD") && OBRS_SINGLE_IPAD) { ?>
                <div id="menu">
                    <div class="button-single button-single-menu button-single-left" id="reset" onclick="resetRoom()">
                        <?php echo $stringsGeneral->getString('RESET'); ?>
                    </div>
                    <div class="button-single button-single-menu button-single-right" id="logout"><a
                            href="../setup/?logout=1">
                            <?php echo $stringsGeneral->getString('LOGOUT'); ?>
                        </a></div>
                </div>
            <?php } ?>
            <div id="retry" class="button-single" onclick="cancelRecordingRequest()">
                <?php echo $strings->getString('RETRY'); ?>
            </div>
            <div class="overlayContainer" id="retryConfirm">
                <div class="overlayContainerInner">
                    <div id="retryConfirmInner">
                        <?php echo $strings->getString('RETRY_CONFIRM'); ?>
                    </div>
                    <div><button onclick="cancelRecording()">
                            <?php echo $strings->getString('RETRY_CONFIRM_SUBMIT'); ?>
                        </button><button onclick="cancelRecordingCancel()">
                            <?php echo $strings->getString('RETRY_CONFIRM_CANCEL'); ?>
                        </button></div>
                </div>
            </div>
        </div>

        <div id="video_preview">
            <div id="video_preview_inner">
                <video id="video" preload="none" controls playsinline>
                    <source id="videosource" src="" type="<?php echo OBRS_VIDEO_FILE_MIME; ?>">
                </video>
                <div>
                    <div class="button-single video-button video-button-red button-single-left"
                        onclick="deleteRecording()">
                        <?php echo $strings->getString('DELETE'); ?>
                    </div>
                    <div class="button-single video-button video-button-green button-single-right"
                        onclick="saveRecording()">
                        <?php echo $strings->getString('SAVE'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="overlayContainer" id="errorMsg">
        <div class="overlayContainerInner">
            <div id="errorMsgInner">Fehler</div>
            <div><button onclick="window.location.reload();">OK</button></div>
        </div>
    </div>

    <script>
        var validateRecordingActive = <?php echo (defined("OBRS_VALIDATE_RECORDING") && defined("OBRS_VALIDATE_RECORDING_START") && defined("OBRS_VALIDATE_RECORDING_IGNORE") && OBRS_VALIDATE_RECORDING ? "true" : "false"); ?>;
    </script>
    <script src="<?php echo OBRS_WEB_JS_ROOM; ?>"></script>

</body>

</html>
