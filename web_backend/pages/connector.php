<?php

/**
 * AJAX Backend for Client requests
 */

/**
 * Log Files
 */
if (defined("OBRS_LOG_PATH")) {
    $setupLogFile = OBRS_LOG_PATH . "/error_setup.log";
    if (!file_exists($setupLogFile)) {
        touch($setupLogFile);
    }
    $roomLogFile = OBRS_LOG_PATH . "/error_room.log";
    if (!file_exists($roomLogFile)) {
        touch($roomLogFile);
    }
    $roomPreviewLogFile = OBRS_LOG_PATH . "/error_room_preview.log";
    if (!file_exists($roomPreviewLogFile)) {
        touch($roomPreviewLogFile);
    }
}

/**
 * Perform tasks
 */
if ($profile == 0) {
    /**
     * Only reset environment and proceed
     */
    echo exec("DISPLAY=:0 $scenesLocation/clean.sh");
    exit;
} else if ($profile < 100) {
    /**
     * Choose scene
     */
    if (!array_key_exists($profile, OBRS_PROFILE_KEYS)) {
        echo $strings->getString('SETUP_PROFILE1');
        exit;
    }
    $key = OBRS_PROFILE_KEYS[$profile];

    /**
     * Lights (DMX)
     */
    if (defined("OBRS_NO_LIGHTS") && OBRS_NO_LIGHTS) {
        $dmx = -1;
    } else if (defined("OBRS_DMX_VALUES")) {
        $dmx = OBRS_DMX_VALUES;
    } else {
        $dmx = "255";
    }

    $command = "DISPLAY=:0 $scenesLocation/switchscenario.sh -d $dmx -k $key";
    exec($command, $returnText, $returnSuccess);
    if ($returnSuccess == 0) {
        echo $profile;
    } else {
        echo $strings->getString('SETUP_PROFILE2', $returnSuccess);
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": '$command': " . $strings->getString('SETUP_PROFILE2', $returnSuccess) . PHP_EOL, FILE_APPEND);
        }
    }
    exit;
} else if ($profile < 2000) {
    /**
     * Choose background
     */
    $command = "$scenesLocation/switchbackground.sh -b $profile";
    exec($command, $returnText, $returnSuccess);
    if ($returnSuccess == 0) {
        echo $profile;
    } else {
        echo $strings->getString('SETUP_BACKGROUND', $returnSuccess);
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": '$command': " . $strings->getString('SETUP_BACKGROUND', $returnSuccess) . PHP_EOL, FILE_APPEND);
        }
    }
    exit;
} else if ($profile == 2000) {
    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();
    if ($pdo->isSessionActive()) {
        /**
         * Start recording
         */
        exec("DISPLAY=:0 $scenesLocation/recordstart.sh", $returnText, $returnSuccess);
        if ($returnSuccess == 0) {
            echo 2000;
        } else {
            echo $strings->getString('ROOM_RECORD');
            if (defined("OBRS_LOG_PATH") && file_exists($roomLogFile) && is_file($roomLogFile)) {
                file_put_contents($roomLogFile, date("Y-m-d H:i:s") . ": Start Recording ($profile): " . $strings->getString('ROOM_RECORD') . PHP_EOL, FILE_APPEND);
            }
        }
    } else {
        echo $strings->getString('ROOM_LOGIN');
        if (defined("OBRS_LOG_PATH") && file_exists($roomLogFile) && is_file($roomLogFile)) {
            file_put_contents($roomLogFile, date("Y-m-d H:i:s") . ": Start Recording ($profile): " . $strings->getString('ROOM_LOGIN') . PHP_EOL, FILE_APPEND);
        }
    }
    exit;
} else if ($profile == 2001) {
    /**
     * Stop recording
     */
    exec("DISPLAY=:0 $scenesLocation/recordstop.sh", $returnText, $returnSuccess);
    if ($returnSuccess == 0) {
        echo 2001;
    } else {
        echo $strings->getString('ROOM_RECORD');
        if (defined("OBRS_LOG_PATH") && file_exists($roomLogFile) && is_file($roomLogFile)) {
            file_put_contents($roomLogFile, date("Y-m-d H:i:s") . ": Stop Recording ($profile): " . $strings->getString('ROOM_RECORD') . PHP_EOL, FILE_APPEND);
        }
    }
    exit;
} else if ($profile == 2002) {
    /**
     * Validate recording
     */
    if (defined("OBRS_VALIDATE_RECORDING") && defined("OBRS_VALIDATE_RECORDING_START") && defined("OBRS_VALIDATE_RECORDING_IGNORE") && OBRS_VALIDATE_RECORDING) {
        $command = sprintf("$scenesLocation/recordvalidation.sh -f '%s' -a '%s'", OBRS_VALIDATE_RECORDING_START, OBRS_VALIDATE_RECORDING_IGNORE);
        exec($command, $returnText, $returnSuccess);
        if ($returnSuccess == 0) {
            echo 2002;
        } else {
            echo $strings->getString('ROOM_RECORD');
            if (defined("OBRS_LOG_PATH") && file_exists($roomLogFile) && is_file($roomLogFile)) {
                file_put_contents($roomLogFile, date("Y-m-d H:i:s") . ": Validate Recording ($profile): '$command': Error $returnSuccess: " . $strings->getString('ROOM_RECORD') . PHP_EOL, FILE_APPEND);
            }
            exec("DISPLAY=:0 $scenesLocation/recordstop.sh", $returnText, $returnSuccess);
        }
    } else {
        echo 2002;
    }
    exit;
} else if ($profile == 3000) {
    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();
    /**
     * Get Last Video
     */
    $videofname = \NetzwerkMedienObrs\getLatestVideo();
    $status = 0;
    $message = $videofname;
    if ($pdo->doesVideoExist($videofname)) {
        $status = 1;
        $message = $strings->getString('ROOM_VIDEO');
    } else if (!file_exists(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $videofname)) {
        $status = 2;
        $message = $strings->getString('ROOM_FILE');
    } else if (!$pdo->isSessionActive()) {
        $status = 3;
        $message = $strings->getString('ROOM_LOGIN');
    } else if (filectime(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $videofname) - date('Z') < strtotime($pdo->getCurrentSession()->sessiontime)) {
        $status = 4;
        $message = $strings->getString('ROOM_LOGIN');
    }
    echo "{\"status\": \"$status\", \"message\": \"$message\"}";
    if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile) && $status !== 0) {
        file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Get Latest Recording ($profile): Error $status: $message" . PHP_EOL, FILE_APPEND);
    }
    exit;
} else if ($profile == 3001) {
    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();
    /**
     * Save video
     */
    $videofname = \NetzwerkMedienObrs\getLatestVideo();
    if ($pdo->doesVideoExist($videofname)) {
        echo $strings->getString('ROOM_VIDEO');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Save Latest Recording ($profile): " . $strings->getString('ROOM_VIDEO') . PHP_EOL, FILE_APPEND);
        }
        exit;
    } else if (!file_exists(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $videofname)) {
        echo $strings->getString('ROOM_FILE');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Save Latest Recording ($profile): " . $strings->getString('ROOM_FILE') . PHP_EOL, FILE_APPEND);
        }
        exit;
    } else if (!$pdo->isSessionActive()) {
        echo $strings->getString('ROOM_LOGIN');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Save Latest Recording ($profile): " . $strings->getString('ROOM_LOGIN') . PHP_EOL, FILE_APPEND);
        }
        exit;
    } else if (filectime(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $videofname) - date('Z') < strtotime($pdo->getCurrentSession()->sessiontime)) {
        echo $strings->getString('ROOM_LOGIN');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Save Latest Recording ($profile): " . $strings->getString('ROOM_LOGIN') . PHP_EOL, FILE_APPEND);
        }
        exit;
    }
    if ($pdo->insertVideo($videofname)) {
        /**
         * Generate Preview
         */
        \NetzwerkMedienObrs\generateVideoPreview();
        echo 3001;
    } else {
        echo $strings->getString('ROOM_SAVE');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Save Latest Recording ($profile): " . $strings->getString('ROOM_SAVE') . PHP_EOL, FILE_APPEND);
        }
    }
    exit;
} else if ($profile == 3002) {
    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();

    /**
     * Delete video
     */
    $videofname = \NetzwerkMedienObrs\getLatestVideo();
    if ($pdo->doesVideoExist($videofname)) {
        echo $strings->getString('ROOM_VIDEO');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Delete Latest Recording ($profile): " . $strings->getString('ROOM_VIDEO') . PHP_EOL, FILE_APPEND);
        }
        exit;
    } else if (!file_exists(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $videofname)) {
        echo $strings->getString('ROOM_FILE');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Delete Latest Recording ($profile): " . $strings->getString('ROOM_FILE') . PHP_EOL, FILE_APPEND);
        }
        exit;
    } else if (!$pdo->isSessionActive()) {
        echo $strings->getString('ROOM_LOGIN');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Delete Latest Recording ($profile): " . $strings->getString('ROOM_LOGIN') . PHP_EOL, FILE_APPEND);
        }
        exit;
    } else if (filectime(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $videofname) - date('Z') < strtotime($pdo->getCurrentSession()->sessiontime)) {
        echo $strings->getString('ROOM_LOGIN');
        if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
            file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Delete Latest Recording ($profile): " . $strings->getString('ROOM_LOGIN') . PHP_EOL, FILE_APPEND);
        }
        exit;
    }

    if (rename(OBRS_SHELL_MEDIA_PATH_RECORDS . '/' . $videofname, OBRS_SHELL_MEDIA_PATH_RECORDS_TRASH . "/" . $videofname)) {
        echo 3002;
        exit;
    }
    echo $strings->getString('ROOM_TASK');
    if (defined("OBRS_LOG_PATH") && file_exists($roomPreviewLogFile) && is_file($roomPreviewLogFile)) {
        file_put_contents($roomPreviewLogFile, date("Y-m-d H:i:s") . ": Delete Latest Recording ($profile): " . $strings->getString('ROOM_TASK') . PHP_EOL, FILE_APPEND);
    }
    exit;
} else if ($profile == 4000) {
    /**
     * Keep connection alive
     */
    echo 4000;
    exit;
} else if ($profile == 5000) {
    /**
     * Choose User background
     */
    $bg = preg_replace('/[^a-zA-Z0-9.]/', '', strval($_POST['additionalParam']));

    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    if (!$pdo->connect()) {
        echo $strings->getString('SETUP_SERVER');
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": $profile: " . $strings->getString('SETUP_SERVER') . PHP_EOL, FILE_APPEND);
        }
        exit;
    }
    $email = $pdo->getCurrentSession()->userid;
    if (empty($email)) {
        echo $strings->getString('SETUP_LOGIN');
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": $profile: " . $strings->getString('SETUP_LOGIN') . PHP_EOL, FILE_APPEND);
        }
        exit;
    }
    if (!$pdo->doesBackgroundBelongToUser($email, $bg)) {
        echo $strings->getString('SETUP_AUTH');
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": $profile: " . $strings->getString('SETUP_AUTH') . PHP_EOL, FILE_APPEND);
        }
        exit;
    }

    $bg = preg_replace('/\.png$/', '', $bg);
    $command = "$scenesLocation/switchbackground.sh -b $bg -u 1";
    exec($command, $returnText, $returnSuccess);
    if ($returnSuccess === 0) {
        echo 5000;
    } else {
        echo $strings->getString('SETUP_SERVER');
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": $profile: '$command': " . $strings->getString('SETUP_SERVER') . PHP_EOL, FILE_APPEND);
        }
    }
    exit;
} else if ($profile == 5001) {
    /**
     * Get User backgrounds
     */

    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    if (!$pdo->connect()) {
        echo '{"status": "1", "message": "' . $strings->getString('SETUP_SERVER') . '"}';
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": $profile: " . $strings->getString('SETUP_SERVER') . PHP_EOL, FILE_APPEND);
        }
        exit;
    }
    $email = $pdo->getCurrentSession()->userid;
    if (empty($email)) {
        echo '{"status": "2", "message": "' . $strings->getString('SETUP_LOGIN') . '"}';
        if (defined("OBRS_LOG_PATH") && file_exists($setupLogFile) && is_file($setupLogFile)) {
            file_put_contents($setupLogFile, date("Y-m-d H:i:s") . ": $profile: " . $strings->getString('SETUP_LOGIN') . PHP_EOL, FILE_APPEND);
        }
        exit;
    }
    $picsFromUser = $pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_FINAL);
    $picsFromUserNames = array();
    foreach ($picsFromUser as $pic) {
        array_push($picsFromUserNames, $pic["filename"]);
    }
    $json = json_encode(["status" => 0, "files" => $picsFromUserNames, "basePath" => OBRS_WEB_PATH_BACKGROUNDS_SETUP . "/"]);
    echo $json;
    exit;
}
