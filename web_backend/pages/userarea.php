<?php

/**
 * User area backend
 */

?>
<!doctype html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $stringsUserArea->getString('NAME'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
	<style>
		#logo img {
			width: 75vw;
			height: 75vw;
			max-width: 200px;
			max-height: 200px;
			border-radius: 100px;
		}

		#welcome a {
			color: inherit;
			text-decoration: none;
		}
	</style>
	<script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
</head>

<body>
	<?php
	$nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
	$nav->getHTML();
	?>
	<main>
		<article id="logo">
			<div class="article-main">
				<img src="/img/logo.png" alt="Logo OBRS">
			</div>
		</article>
		<article id="welcome">
			<header>
				<h1><?php echo $stringsUserArea->getString('HOME_WELCOME', $email); ?></h1>
			</header>
			<div class="article-main">
				<p><?php echo $stringsUserArea->getString('HOME_WELCOME_INTRO'); ?></p>
				<p><a href="<?php echo OBRS_LINK_USER_AREA; ?>/videoarchive/"><i><?php echo $stringsUserArea->getString('AREA_VIDEOARCHIVE'); ?></i>: <?php echo $stringsUserArea->getString('HOME_WELCOME_AREA_VIDEOARCHIVE_DESCRIPTION'); ?></a></p>
				<p><a href="<?php echo OBRS_LINK_USER_AREA; ?>/hintergrund/"><i><?php echo $stringsUserArea->getString('AREA_PICS'); ?></i>: <?php echo $stringsUserArea->getString('HOME_WELCOME_AREA_PICS_DESCRIPTION'); ?></a></p>
				<p><?php echo $stringsUserArea->getString('HOME_WELCOME_NAVIGATION_NOTICE'); ?></p>
			</div>
		</article>
	</main>
</body>

</html>
