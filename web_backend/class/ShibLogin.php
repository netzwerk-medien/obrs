<?php

namespace NetzwerkMedienObrs;

/**
 * Shibboleth connnection
 */
class ShibLogin
{


	/**
	 * @var array Server variables
	 */
	private $server;

	/**
	 * @var string|boolean User Display Name
	 */
	private $displayName = false;
	/**
	 * @var string|boolean User eMail Address
	 */
	private $email = false;
	/**
	 * @var string|boolean User Alias
	 */
	private $alias = false;
	/**
	 * @var string Error Message
	 */
	private $errorMsg = '';

	function __construct($server)
	{
		$this->server = $server;
	}

	/**
	 * Perform login
	 * @return int Status Code
	 */
	function login()
	{
		/**
		 * @var bool Is user logged in?
		 */
		$shibloggedin = false;
		if ((defined("OBRS_LOGIN_HANDLER_INTERNAL") && OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL) || OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP) {
			if (session_status() !== PHP_SESSION_ACTIVE) {
				session_start();
			}
			if (isset($_SESSION['token']) && isset($_COOKIE['token']) && strcmp($_SESSION['token'], $_COOKIE['token']) === 0) {
				$this->email = $_SESSION['mail'];
				$this->alias = $_SESSION['userid'];
				$this->displayName = $_SESSION['username'];
				return 0;
			}
		} else {
			/**
			 * @var bool Is Shib-Identity-Provider available?
			 */
			$shibidentity = false;
			/**
			 * @var bool Is user authorized?
			 */
			$affiliation = false;

			if (array_key_exists('Shib-Identity-Provider', $this->server)) {
				if ($this->server["Shib-Identity-Provider"] == OBRS_SHIB_ID_PROVIDER) {
					$shibloggedin = true;
					if (array_key_exists('mail', $this->server)) {
						if (filter_var(trim($this->server["mail"]), FILTER_VALIDATE_EMAIL)) {
							if (array_key_exists('affiliation', $this->server)) {
								foreach (OBRS_SHIB_AFFILIATIONS as $affiliationValidate) {
									if (strpos($this->server["affiliation"], $affiliationValidate) !== false) {
										$shibidentity = $this->server["Shib-Identity-Provider"];
										$this->email = trim($this->server["mail"]);
										$emailsplit = explode("@", $this->email);
										if (is_array($emailsplit)) {
											$this->alias = $emailsplit[0];
										}
										$this->displayName = trim($this->server["displayName"]);
										$affiliation = trim($this->server["affiliation"]);
										break;
									}
								}
							}
						}
					}
				}
			}
			if ($shibloggedin && ($shibidentity === false || $this->email === false || $affiliation === false || $this->displayName === false)) {
				if ($affiliation === false) {
					$this->errorMsg = OBRS_SHIB_AFFILIATION_ERROR;
				} else if ($this->email === false) {
					$this->errorMsg = OBRS_SHIB_MAIL_ERROR;
				} else if ($this->displayName === false) {
					$this->errorMsg = OBRS_SHIB_NAME_ERROR;
				}
				return 1;
			} else if ($shibloggedin) {
				return 0;
			}
		}
		$this->errorMsg = OBRS_SHIB_LOGIN_ERROR;
		return 2;
	}

	/**
	 * Get Error Message
	 * @return string Error Message
	 */
	function getErrorMsg()
	{
		return $this->errorMsg;
	}

	/**
	 * Get User Display Name
	 * @return string|boolean User Display Name
	 */
	function getDisplayName()
	{
		return $this->displayName;
	}

	/**
	 * Get User eMail Address
	 * @return string|boolean User eMail Address
	 */
	function getMail()
	{
		return $this->email;
	}

	/**
	 * Get User Alias
	 * @return string|boolean User Alias
	 */
	function getAlias()
	{
		return $this->alias;
	}
}
