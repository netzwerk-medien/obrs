<?php

namespace NetzwerkMedienObrs;

/**
 * Navigation for Admin area
 */
class AdminAreaNavigation {

	/**
	 * @var object navigation strings
	 */
	private $strings;

	/**
	 * Construct with strings
	 * @param object $strings
	 */
	function __construct($strings) {
		$this->strings=$strings;
	}

	/**
	 * Generate and output HTML
	 * @return void
	 */
	function getHTML(){
		if(is_object($this->strings)){
			?>
			<!-- Main Menu -->
			<nav id="morenav" class="hidden">
					<ul>
						<li><a href="<?php echo htmlspecialchars(OBRS_LINK_ADMIN_AREA); ?>/?logout=1"><i class="material-icons">power_settings_new</i><span><?php echo $this->strings->getString('NAV_LOGOUT'); ?></span></a></li>
					</ul>
			</nav>
			<!-- Top Navigation -->
			<nav id="topnav">
				<a href="<?php echo htmlspecialchars(OBRS_LINK_ADMIN_AREA); ?>/"><?php echo $this->strings->getString('AREA_HOME'); ?></a><span> | </span><a href="<?php echo htmlspecialchars(OBRS_LINK_ADMIN_AREA); ?>/videomgr/"><?php echo $this->strings->getString('AREA_VIDEOMANAGER'); ?></a><i id="topnav-toggle" onclick="showMoreNav()" class="material-icons">more_vert</i>
			</nav><?php
		}
	}
}
