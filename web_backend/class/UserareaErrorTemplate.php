<?php
/**
 * Login Template class
 */

namespace NetzwerkMedienObrs;

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once (OBRS_BASE_PATH . "/shared/includes.php");
require_once (OBRS_BASE_PATH . '/class/UserAreaNavigation.php');

/**
 * Login Template for all login areas
 */
class UserareaErrorTemplate
{

    /**
     * @var string Error Message
     */
    private $text;

    /**
     * Define login area
     * @param string $text
     */
    function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * Generate and output HTML
     * @return void
     */
    function getHTML()
    {

        $stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
        if ($stringsGeneral->hasErrors()) {
            echo OBRS_I18N_ERROR;
            throw new \Exception($stringsGeneral->listErrors());
        }
        $stringsUserArea = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_USER_AREA);
        if ($stringsUserArea->hasErrors()) {
            echo OBRS_I18N_ERROR;
            throw new \Exception($stringsUserArea->listErrors());
        }
        ?>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>
                <?php echo $stringsGeneral->getString('ERROR'); ?>
            </title>
            <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
            <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
            <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_USER_AREA; ?>">
            <script src="<?php echo OBRS_WEB_JS_USER_AREA; ?>"></script>
            <style>
                main {
                    text-align: center;
                    padding: 0.5em;
                }

                #error {
                    font-size: 2em;
                    color: darkred;
                    font-weight: bold;
                }
            </style>
        </head>

        <body>
            <?php
            $nav = new \NetzwerkMedienObrs\UserAreaNavigation($stringsUserArea);
            echo $nav->getHTML();
            ?>
            <main>
                <h1>
                    <?php echo $stringsGeneral->getString('ERROR'); ?>
                </h1>
                <div id="error">
                    <?php echo $this->text; ?>
                </div>
            </main>
        </body>

        </html>
        <?php
    }
}
