<?php

namespace NetzwerkMedienObrs;

/**
 * SQLite connnection
 */
class Sqlite
{

    /**
     * @var \PDO PDO instance
     */
    private $pdo;

    /**
     * Return an instance of the PDO object that connects to the SQLite database
     * @return \PDO
     */
    public function connect()
    {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . OBRS_BASE_PATH . "/database/obrs.sqlite.db");
        }
        return $this->pdo;
    }

    /**
     * Insert a new session into the sessions table
     * @param string $userid unique name of user
     * @return int ID of the new session
     */
    public function insertSession($userid)
    {
        $sql = "INSERT INTO sessions(userid,session_active) VALUES(:userid,:session_active)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':userid', $userid);
        $stmt->bindValue(':session_active', 1);
        $stmt->execute();
        return $this->pdo->lastInsertId();
    }

    /**
     * Set current session stop time on logout
     * @return boolean success
     */
    public function endCurrentSession()
    {
        $sql = "SELECT session_id,sessiontimeend FROM sessions ORDER BY sessiontime DESC LIMIT 1";
        $stmt = $this->pdo->query($sql);
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (empty($row) || empty($row["sessiontimeend"])) {
            $sql = "UPDATE sessions SET sessiontimeend=CURRENT_TIMESTAMP WHERE session_id=:session_id LIMIT 1";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':session_id', $row["session_id"]);
            return $stmt->execute();
        }
        return true;
    }

    /**
     * Deactivate all sessions on logout
     * @return boolean success
     */
    public function endSessions()
    {
        $sql = "UPDATE sessions SET session_active=0";
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute();
    }

    /**
     * Get all sessions
     * @return array Array containing the session list
     */
    public function getSessions()
    {
        $stmt = $this->pdo->query('SELECT session_id,userid,sessiontime,sessiontimeend,session_active FROM sessions ORDER BY sessiontime ASC');
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Get current session
     * @return object Object containing the session
     */
    public function getCurrentSession()
    {
        $stmt = $this->pdo->query('SELECT * FROM sessions ORDER BY sessiontime DESC LIMIT 1');
        $session = $stmt->fetch(\PDO::FETCH_OBJ);
        return $session;
    }

    /**
     * Is current session active?
     * @return boolean state active
     */
    public function isSessionActive()
    {
        $session = $this->getCurrentSession();
        $active = boolval($session->session_active);
        if ($active && time() - strtotime($session->sessiontime) > OBRS_AUTO_LOGOUT_TIME) {
            $this->endCurrentSession();
            $this->endSessions();
            return false;
        }
        return $active;
    }

    /**
     * Get Logins
     * @param string $userid unique name of user
     * @return array Logins
     */
    public function getLogins($userid)
    {
        $stmt = $this->pdo->prepare('SELECT session_id,userid,sessiontime,sessiontimeend,session_active FROM sessions WHERE userid=:userid');
        $stmt->bindValue(':userid', $userid);
        $stmt->execute();
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Get Last Login
     * @param string $userid unique name of user
     * @return object Last Login
     */
    public function getLastLogin($userid)
    {
        $stmt = $this->pdo->prepare('SELECT userid,session_id,sessiontime,sessiontimeend FROM sessions WHERE session_active=0 AND userid=:userid ORDER BY sessiontime DESC LIMIT 1');
        $stmt->bindValue(':userid', $userid);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * Insert a new video into the videos table
     * @param string $filename
     * @param object|null $session
     * @return boolean success
     */
    public function insertVideo($filename, $session = null)
    {
        if ($session === null) {
            $session = $this->getCurrentSession();
        }
        if (is_object($session) && !empty($session->userid) && !empty($session->session_id)) {
            $sql = "INSERT INTO videos(session_id,userid,filename) VALUES(:session_id,:userid,:filename)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':session_id', $session->session_id);
            $stmt->bindValue(':userid', $session->userid);
            $stmt->bindValue(':filename', $filename);
            return $stmt->execute();
        }
        return false;
    }

    /**
     * Get all videos associated with an email address
     * @param string $userid unique name of user
     * @return array Array containing the video list
     */
    public function getVideoList($userid)
    {
        $sql = "SELECT video_id,session_id,filename,userid,videosavetime FROM videos WHERE userid=:userid AND videosavetime BETWEEN datetime('now', :displaytime) AND datetime('now', 'localtime') ORDER BY filename ASC";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':userid', $userid);
        $stmt->bindValue(':displaytime', '-' . OBRS_VIDEO_DISPLAY_TIME . ' days');
        $stmt->execute();
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Get all videos
     * @return array Array containing the video list
     */
    public function getAllVideos()
    {
        $sql = "SELECT video_id,session_id,filename,userid,videosavetime FROM videos ORDER BY videosavetime ASC";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Does Video exist?
     * @param string $filename name of file
     * @return boolean exists
     */
    public function doesVideoExist($filename)
    {
        $sql = "SELECT 1 FROM videos WHERE filename=:filename LIMIT 1";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':filename', $filename);
        $stmt->execute();
        return boolval($stmt->fetchColumn());
    }

    /**
     * Insert a new background into the backgrounds table
     * @param string $userid unique name of user
     * @param int $state predefined state of background
     * @param string $filename name of file
     * @return boolean success
     */
    public function insertBackground($userid, $state, $filename)
    {
        if (!empty($userid)) {
            $sql = "INSERT INTO backgrounds(userid,filename,state) VALUES(:userid,:filename,:state)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->bindValue(':state', intval($state));
            $stmt->bindValue(':filename', $filename);
            return $stmt->execute();
        }
        return false;
    }

    /**
     * Update a background state
     * @param string $userid unique name of user
     * @param int $state predefined state of background
     * @param string $filename name of file
     * @return boolean success
     */
    public function updateBackgroundState($userid, $state, $filename)
    {
        if (!empty($userid)) {
            $sql = "UPDATE backgrounds SET state=:state WHERE userid=:userid AND filename=:filename";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->bindValue(':state', intval($state));
            $stmt->bindValue(':filename', $filename);
            return $stmt->execute();
        }
        return false;
    }

    /**
     * Get all backgrounds associated with unique name of user
     * @param string $userid unique name of user
     * @param int $state predefined state of background
     * @return array Array containing the background list
     */
    public function getBackgroundList($userid, $state)
    {
        $sql = "SELECT background_id,filename,state,userid,backgroundsavetime FROM backgrounds WHERE userid=:userid AND state=:state ORDER BY backgroundsavetime ASC";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':userid', $userid);
        $stmt->bindValue(':state', intval($state));
        $stmt->execute();
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Get all backgrounds
     * @return array Array containing the background list
     */
    public function getAllBackgrounds()
    {
        $sql = "SELECT background_id,filename,state,userid,backgroundsavetime FROM backgrounds ORDER BY backgroundsavetime ASC";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Does Background belong to user?
     * @param string $userid unique name of user
     * @param string $filename name of file
     * @return boolean Does Background belong to user?
     */
    public function doesBackgroundBelongToUser($userid, $filename)
    {
        $sql = "SELECT 1 FROM backgrounds WHERE userid=:userid AND filename=:filename LIMIT 1";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':userid', $userid);
        $stmt->bindValue(':filename', $filename);
        $stmt->execute();
        return boolval($stmt->fetchColumn());
    }

    /**
     * Exists Internal Auth user
     * @param string $userid unique name of user
     * @return boolean exists
     */
    public function existsInternalAuthUser($userid)
    {
        if (!empty($userid)) {
            $sql = "SELECT 1 FROM users WHERE userid=:userid COLLATE NOCASE LIMIT 1";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->execute();
            return boolval($stmt->fetchColumn());
        }
        return false;
    }

    /**
     * Insert Internal Auth user
     * @param string $userid unique name of user
     * @param string $pw password
     * @return boolean success
     */
    public function insertInternalAuthUser($userid, $pw)
    {
        if (!empty($userid) && $userid === preg_replace(OBRS_INTERNAL_AUTH_LOGIN_USERNAME_PATTERN, "", $userid) && !empty($pw) && !$this->existsInternalAuthUser($userid)) {
            $pw = password_hash($pw, PASSWORD_DEFAULT);
            $sql = "INSERT INTO users (userid, password) VALUES (:userid,:password)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->bindValue(':password', $pw);
            return $stmt->execute();
        }
        return false;
    }

    /**
     * Validate Internal Auth user
     * @param string $userid unique name of user
     * @param string $pw password
     * @return boolean success
     */
    public function validateInternalAuthUser($userid, $pw)
    {
        if (!empty($userid) && $userid === preg_replace(OBRS_INTERNAL_AUTH_LOGIN_USERNAME_PATTERN, "", $userid) && !empty($pw)) {
            $sql = "SELECT userid,password FROM users WHERE userid=:userid LIMIT 1";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            if ($stmt->execute()) {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return password_verify($pw, $row["password"]);
            }
        }
        return false;
    }

    /**
     * Get all internal auth users
     * @return array Array containing the users list
     */
    public function getUsers()
    {
        $stmt = $this->pdo->query('SELECT auto_id,userid FROM users ORDER BY userid ASC');
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }


    /**
     * Is user admin?
     * @param string $userid unique name of user
     * @return boolean admin
     */
    public function isAdmin($userid)
    {
        if (!empty($userid)) {
            $sql = "SELECT 1 FROM admins WHERE userid=:userid LIMIT 1";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->execute();
            return boolval($stmt->fetchColumn());
        }
        return false;
    }

    /**
     * Get admin auth level
     * @param string $userid unique name of user
     * @return int level
     */
    public function adminAuthLevel($userid)
    {
        if (!empty($userid)) {
            $sql = "SELECT auth_level FROM admins WHERE userid=:userid LIMIT 1";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->execute();
            return $stmt->fetchColumn();
        }
        return 0;
    }

    /**
     * Create admin
     * @param string $userid unique name of user
     * @param int level
     * @return boolean success
     */
    public function createAdmin($userid, $level)
    {
        if (!empty($userid) && !empty($level) && !$this->isAdmin($userid)) {
            $sql = "INSERT INTO admins (userid, auth_level) VALUES (:userid,:auth_level)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->bindValue(':auth_level', intval($level));
            return $stmt->execute();
        }
        return false;
    }

    /**
     * Change admin level
     * @param string $userid unique name of user
     * @param int level
     * @return boolean success
     */
    public function changeAdminLevel($userid, $level)
    {
        if (!empty($userid) && $this->isAdmin($userid)) {
            $sql = "UPDATE admins SET auth_level=:auth_level WHERE userid=:userid";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':userid', $userid);
            $stmt->bindValue(':auth_level', intval($level));
            return $stmt->execute();
        }
        return false;
    }

    /**
     * Get all admins
     * @return array Array containing the admin list
     */
    public function getAdmins()
    {
        $stmt = $this->pdo->query('SELECT auto_id,userid,auth_level FROM admins ORDER BY auth_level DESC, userid ASC');
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Get all tables
     * @return array Array containing the table list
     */
    public function getTables()
    {
        $stmt = $this->pdo->query('SELECT name FROM sqlite_master  WHERE type=\'table\' ORDER BY name');
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $row['name'];
        }
        return $data;
    }
}
