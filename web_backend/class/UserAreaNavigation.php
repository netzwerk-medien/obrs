<?php

namespace NetzwerkMedienObrs;

/**
 * Navigation for User area
 */
class UserAreaNavigation
{


	/**
	 * @var object navigation strings
	 */
	private $strings;

	/**
	 * Construct with strings
	 * @param object $strings
	 */
	function __construct($strings)
	{
		$this->strings = $strings;
	}

	/**
	 * Generate and output HTML
	 * @return void
	 */
	function getHTML()
	{
		if (is_object($this->strings)) {
?>
			<!-- Main Menu -->
			<nav id="morenav" class="hidden">
				<ul>
					<li><a href="<?php echo htmlspecialchars(OBRS_LINK_HELP); ?>" target="_blank"><i class="material-icons">book</i><span><?php echo $this->strings->getString('NAV_ABOUT'); ?></span></a></li>
					<li><a href="<?php echo htmlspecialchars(OBRS_LINK_USER_AREA); ?>/lizenzen/"><i class="material-icons">info</i><span><?php echo $this->strings->getString('AREA_LICENSE'); ?></span></a></li>
					<li><a href="<?php echo htmlspecialchars(OBRS_LINK_USER_AREA); ?>/?logout=1"><i class="material-icons">power_settings_new</i><span><?php echo $this->strings->getString('NAV_LOGOUT'); ?></span></a></li>
				</ul>
			</nav>
			<!-- Top Navigation -->
			<nav id="topnav">
				<a href="<?php echo htmlspecialchars(OBRS_LINK_USER_AREA); ?>/"><?php echo $this->strings->getString('AREA_HOME'); ?></a><span> | </span><a href="<?php echo htmlspecialchars(OBRS_LINK_USER_AREA); ?>/videoarchive/"><?php echo $this->strings->getString('AREA_VIDEOARCHIVE'); ?></a><span> | </span><a href="<?php echo htmlspecialchars(OBRS_LINK_USER_AREA); ?>/hintergrund/"><?php echo $this->strings->getString('AREA_PICS'); ?></a><i id="topnav-toggle" onclick="showMoreNav()" class="material-icons">more_vert</i>
			</nav><?php
				}
			}
		}
