<?php

namespace NetzwerkMedienObrs;

/**
 * Login Template for all login areas
 */
class LoginTemplate
{

    /**
     * @var string Webpage Head Title
     */
    private $head_title;

    /**
     * @var string Webpage Body Title
     */
    private $body_header;

    /**
     * @var string|array Webpage Body Text
     */
    private $body_text;

    /**
     * @var string Login Link
     */
    private $body_link;

    /**
     * @var string CSS Style Class
     */
    private $body_class;

    /**
     * @var string Logout Link
     */
    private $body_link_logout;

    /**
     * @var boolean Is it a PWA?
     */
    private $pwa;

    /**
     * Define login area
     * @param string $head_title
     * @param string $body_header
     * @param string|array $body_text
     * @param string $body_link
     * @param string $body_class
     * @param string|null $body_link_logout
     * @param boolean $pwa
     */
    function __construct($head_title, $body_header, $body_text, $body_link, $body_class, $body_link_logout = null, $pwa = false)
    {
        $this->head_title = $head_title;
        $this->body_header = $body_header;
        $this->body_text = $body_text;
        $this->body_link = $body_link;
        $this->body_class = $body_class;
        $this->body_link_logout = $body_link_logout;
        $this->pwa = $pwa;
    }

    /**
     * Generate and output HTML
     * @return void
     */
    function getHTML()
    {

        $stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
        if ($stringsGeneral->hasErrors()) {
            echo OBRS_I18N_ERROR;
            throw new \Exception($stringsGeneral->listErrors());
        }
?>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8">
            <title>
                <?php echo $this->head_title; ?>
            </title>
            <?php
            if ($this->pwa) {
                require_once(OBRS_BASE_PATH . "/tpl/pwa/tags.php");
            } else {
            ?>
                <meta name="viewport" content="width=device-width, initial-scale=1">
            <?php
            } ?>
            <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
            <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_MAIN; ?>">
            <link rel="stylesheet" type="text/css" href="<?php echo OBRS_WEB_CSS_COMMON; ?>">
            <?php if ($this->body_class === "login-setup") { ?>
                <script src="<?php echo OBRS_WEB_JS_SCREENSAVER; ?>"></script>
            <?php } ?>
        </head>

        <body class="<?php echo htmlspecialchars($this->body_class); ?> login">
            <div>
                <div id="background">
                    <h1>
                        <?php echo $this->body_header; ?>
                    </h1>
                    <?php
                    if (gettype($this->body_text) == "array") {
                        foreach ($this->body_text as $text) {
                            echo "<p>" . htmlspecialchars($text) . "</p>";
                        };
                    } else {
                        echo "<p>" . htmlspecialchars($this->body_text) . "</p>";
                    }
                    ?>
                    <div class="button-single" id="login"><a
                            href="<?php echo htmlspecialchars($this->body_link); ?>"><?php echo $stringsGeneral->getString('LOGIN'); ?></a></div>
                </div>
                <?php if (!empty($this->body_link_logout)) { ?>
                    <iframe src="<?php echo htmlspecialchars($this->body_link_logout); ?>" style="width:80%;height:100%;display:none;"></iframe>
                <?php } ?>
            </div>
            <?php if ($this->body_class === "login-setup") { ?>
                <script>
                    if (screensaver) {
                        screensaver.activate();
                    }
                </script>
            <?php } ?>
        </body>

        </html>
<?php
    }
}
