<?php

namespace NetzwerkMedienObrs;


class StringFinder
{

	private $strings = array();
	private $errors = array();

	function __construct($path, $locale, $set)
	{
		$filename = "$path/$locale/$set.obrslang";
		if (file_exists($filename)) {
			$handle = fopen($filename, "r");
			if ($handle) {
				$isID = true;
				$currentID = '';
				while (($reading = fgets($handle)) !== false) {
					if (!empty(str_replace(PHP_EOL, "", $reading))) {
						if (preg_match('/^#/', $reading) !== 1 || !$isID) {
							if ($isID) {
								$currentID = str_replace(PHP_EOL, "", $reading);
							} else {
								$this->strings[$currentID] = str_replace(PHP_EOL, "", $reading);
							}
							$isID = false;
						}
					} else {
						$isID = true;
					}
				}
				fclose($handle);
			}
			if (empty($this->strings)) {
				$this->throwError("Empty string set provided");
			}
		} else {
			$this->throwError("File $filename does not exist.");
		}
	}

	private function encodeString($string, $encode)
	{
		if ($encode == 1) {
			return htmlspecialchars($string);
		}
		return $string;
	}
	function getString($stringID, $args = null, $encode = 1)
	{
		if (array_key_exists($stringID, $this->strings)) {
			$string = ($this->strings)[$stringID];
			$string = preg_replace('/(%)(s|d)/', '%1\$$2', $string);
			$encode = intval($encode);
			if ($encode > 0) {
				$string = $this->encodeString($string, $encode);
			}
			if (is_numeric($args)) {
				$args = array($args);
			} else if (is_string($args)) {
				$args = array($args);
			}
			$count_num = 0;
			$count_string = 0;
			if (is_array($args) && count($args) > 0) {
				foreach ($args as $arg) {
					if (is_numeric($arg)) {
						$count_num++;
						$string = preg_replace('/%' . $count_num . '[$]d/', floatval($arg), $string);
					} else if (is_string($arg)) {
						$count_string++;
						$string = preg_replace('/%' . $count_string . '[$]s/', strval($arg), $string);
					}
				}
			}
			$string = preg_replace('/(%[0-9][$]d)|(%[0-9][$]s)/', '', $string);
			if ($encode < 0) {
				$string = $this->encodeString($string, abs($encode));
			}
			return $string;
		}
		return false;
	}

	function throwError($message)
	{
		array_push($this->errors, $message);
	}

	function hasErrors()
	{
		return count($this->errors) > 0;
	}

	function listErrors($implode = true)
	{
		if (count($this->errors) == 0) {
			return null;
		}
		return $implode ? 'ERRORS: ' . implode(' / ', $this->errors) : $this->errors;
	}

}
