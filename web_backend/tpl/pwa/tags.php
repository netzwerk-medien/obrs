<link rel="manifest" href="pwa/manifest.json" crossorigin="use-credentials">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-title" content="OBRS">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="apple-touch-icon" sizes="72x72" href="pwa/icons/icon-72x72.png" />
<link rel="apple-touch-icon" sizes="96x96" href="pwa/icons/icon-96x96.png" />
<link rel="apple-touch-icon" sizes="128x128" href="pwa/icons/icon-128x128.png" />
<link rel="apple-touch-icon" sizes="144x144" href="pwa/icons/icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="pwa/icons/icon-152x152.png" />
