<?php

/**
 * Login Template Userarea
 */

/**
 * Login Template include
 */
require_once(OBRS_BASE_PATH . "/class/LoginTemplate.php");

/**
 * @var string|null Set Logout Link
 */
$logout = (isset($_GET['logout']) && ((int) $_GET["logout"]) == 1) ? OBRS_SHIB_LOGOUT_URL_USER_AREA : null;
if (OBRS_LOGIN_HANDLER != OBRS_LOGIN_HANDLER_SHIB) {
	header("Location: " . OBRS_SHIB_LOGOUT_URL_USER_AREA);
	exit;
}
/**
 * @var \NetzwerkMedienObrs\loginTemplate Generate HTML for login screen
 */
$login = new \NetzwerkMedienObrs\LoginTemplate(OBRS_SHIB_HEAD_TITLE_USER_AREA, OBRS_SHIB_BODY_TITLE_USER_AREA, OBRS_SHIB_MESSAGE_USER_AREA, OBRS_SHIB_LOGIN_URL_USER_AREA, "login-video", $logout, false);
$login->getHTML();
