# OBRS - One Button Recording Studio

A software to control the OBS (Open Broadcaster Software) via CLI.

## Installation

* Setup Hardware
* Setup Software
* Run install.sh
* Run install.php
* Copy config-sample/ to config/ and set values
* Configure OBS-Studio:
  * Key I = Start recording
  * Key J = Stop recording
  * Key (see below): Set Scenario options (see below); sample in install folder
  * Recording path = /opt/obrsapp/shell_media/records
* Configure Apache Server; sample in install folder
* Minimum PHP Version: 7
* php.ini: file_uploads = On, post_max_size = 8M, upload_max_filesize = 10M (for user backgrounds)

## Scenarios

<table>
    <thead>
        <tr><th>Scene Profile</th><th>Scene Key</th><th>Additional Source</th><th>Background</th></tr>
    </thead>
    <tbody>
         <tr><td>10</td><td>R</td><td>no</td><td>Default / Department</td></tr>
         <tr><td>11</td><td>T</td><td>Document Camera</td><td>Default / Department</td></tr>
         <tr><td>12</td><td>E</td><td>Laptop</td><td>Default / Department</td></tr>
         <tr><td>20</td><td>P</td><td>no</td><td>User</td></tr>
         <tr><td>21</td><td>O</td><td>Document Camera</td><td>User</td></tr>
         <tr><td>22</td><td>U</td><td>Laptop</td><td>User</td></tr>
         <tr><td>30</td><td>Z</td><td>no</td><td>Green Screen</td></tr>
    </tbody>
</table>
