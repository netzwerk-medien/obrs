# CHANGELOG

## 2025-02-03

* JS Screensaver for tablets (mandatory config option: OBRS_WEB_JS_SCREENSAVER)
* New option: Disable DMX lights control by OBRS software (OBRS_NO_LIGHTS)
* Fix option "Validate Recording": Ignore messages that might interfere using OBRS_VALIDATE_RECORDING_IGNORE
* NEW config options (required): OBRS_WEB_JS_SCREENSAVER
* NEW config options (optional): OBRS_NO_LIGHTS, OBRS_VALIDATE_RECORDING_IGNORE

---

## 2024-10-11

* Fetch replaces jQuery
* Nicer error messages at user area
* REMOVED config option: OBRS_WEB_JS_JQUERY

---

## 2024-10-09 (second commit)

* Optional Green Screen Scene
* CHANGED config option: OBRS_PROFILE_KEYS (added green screen scene)
* NEW config option (optional): OBRS_SUPPORT_GREEN_SCREEN

---

## 2024-10-09 (first commit)

* **!!!** Two rooms setup is now removed, some config changes required
* **!!!** Split obrs.js in setup.js and room.js
* REMOVED two rooms setup
* REMOVED config options OBRS_ROOMS, OBRS_SINGLE_ROOM, OBRS_PROFILES (replaced by OBRS_PROFILE_KEYS), OBRS_PROFILES_SINGLE_MODE, OBRS_PROFILES_USER_BG, OBRS_SIMPLIFIED_MENU (now always enabled), OBRS_NO_BUILD_IN_PC (useless in single room setup), OBRS_WEB_JS_MAIN (split), OBRS_WEB_CSS_MAIN (now also for user-/adminarea), OBRS_VALIDATE_RECORDING_END (did not work properly)
* NEW config options (required): OBRS_PROFILE_KEYS, OBRS_WEB_JS_SETUP, OBRS_WEB_JS_ROOM, OBRS_WEB_JS_COMMON, OBRS_WEB_CSS_COMMON
* NEW config options (optional): OBRS_DMX_VALUES

---

## 2022-08-22

* **!!!** Please set the new config options and edit the changed ones in your config file (see below)
* **!!!** Please set the new URL /setup in your (iPad) client
* DEPRECATED Two room setup: in future two room support will be removed in favor of single room setup
* NEW Adminarea: take studio control
* NEW Adminarea: database view
* CHANGED Moved location / to /setup (and renamed web_backend/pages/obrs.php to web_backend/pages/setup.php)
* CHANGED Replaced shell_interface/scenes/lightsoff.sh by shell_interface/scenes/clean.sh
* CHANGED Removed hard-coded setup references to department image web_frontend/img/private_branding/101.png - web_frontend/img/private_branding/106.png by configuration option (see below).
* NEW Config Options:
  * OBRS_SIMPLIFIED_MENU (skip some user menu options at ipad setup),
  * OBRS_DEPARTMENT_BACKGROUND_COUNT (number of department images, references up to 9 images in setup frontend starting from web_frontend/img/private_branding/101.png. Example: If set to '2' 101.png and 102.png will be referenced. OBS background files in shell_media/backgrounds/private_branding/101.png,… and shell_media/backgrounds/private_branding/1101.png,… are also mandatory.),
  * OBRS_ADMIN_LEVEL_CONTROL_STUDIO (admin access level to control studio),
  * OBRS_ADMIN_LEVEL_DB (admin access level to view database details)
* CHANGED Config Options:
  * OBRS_SHIB_LOGIN_URL (move location / to /setup): Change for Shibboleth, LDAP and Internal Auth,
  * OBRS_SHIB_LOGOUT_URL (move location / to /setup): Change for Shibboleth, LDAP and Internal Auth

---

## 2021-08-30

* **!!!** Please run "install/update.php" to create database / directories
* **!!!** Please set the new config options and edit the changed ones in your config file (see below)
* NEW Internal Auth System
* NEW English language sets
* NEW Admin Area
* NEW Log-Files
* NEW Config Options:
  * OBRS_DOCUMENT_CAM (document camera available),
  * OBRS_SINGLE_IPAD (use only one ipad for login and recording),
  * OBRS_LOGIN_HANDLER_INTERNAL (interal auth constant),
  * OBRS_INTERNAL_AUTH_LOGIN_USERNAME_PATTERN (internal auth username validation),
  * OBRS_I18N_SET_ADMIN_AREA (language set admin area),
  * OBRS_I18N_SET_INTERNAL_AUTH (language set internal auth),
  * OBRS_VALIDATE_RECORDING (validate OBS-log checking for record start/stop),
  * OBRS_VALIDATE_RECORDING_START (string expected for start),
  * OBRS_VALIDATE_RECORDING_END (string expected for stop),
  * OBRS_LOG_PATH (path for log files),
  * OBRS_SHIB_LOGIN_URL_ADMIN_AREA (admin area login),
  * OBRS_SHIB_LOGOUT_URL_ADMIN_AREA (admin area login),
  * OBRS_SHIB_HEAD_TITLE_ADMIN_AREA (admin area login),
  * OBRS_SHIB_BODY_TITLE_ADMIN_AREA (admin area login),
  * OBRS_SHIB_MESSAGE_ADMIN_AREA (admin area login),
  * OBRS_LINK_ADMIN_AREA (admin area link),
  * OBRS_SUPER_ADMIN (admin area: Super User like admin),
  * OBRS_ADMIN_LEVEL_GENERAL (admin area: auth level to access admin area),
  * OBRS_ADMIN_LEVEL_VIDEO_MGR (admin area: auth level to allocate videos),
  * OBRS_ADMIN_LEVEL_VIEW_LOGS (admin area: auth level to view logs),
  * OBRS_ADMIN_LEVEL_ADD_ADMIN (admin area: auth level to manage admins )
* CHANGED Config Options:
  * OBRS_SHIB_LOGIN_URL (internal auth added),
  * OBRS_SHIB_LOGOUT_URL (internal auth added),
  * OBRS_SHIB_LOGIN_URL_USER_AREA (internal auth added),
  * OBRS_SHIB_LOGOUT_URL_USER_AREA (internal auth added)

---

## 2020-08-04

* NEW Users can upload own backgrounds.
* NEW Single room enviroment configuration (config options OBRS_SINGLE_ROOM and OBRS_PROFILES_SINGLE_MODE).
* NEW No build-in PC configuration (config option OBRS_NO_BUILD_IN_PC).
* NEW Language files for internationalization (see obrslang.md for details).
* CHANGED Videos and thumbnails are not accessed directly anymore, but handled by a php script.
* CHANGED Auto logout after x seconds. Set in new config option OBRS_AUTO_LOGOUT_TIME.
* CHANGED Videoarchive is now part of the userarea (changed file names / changed config option names).
* CHANGED Apache VHost conf to fit new setup.
* CHANGED Many new config options (see commit file diff for details).
* CHANGED PHP-GD is now required. You need to install it on your machine. It is used for user backgrounds.
* CHANGED Database structure changed. Run update.php in install folder. This script also creates four new folders for user backgrounds.
* CHANGED Config option OBRS_CLIENT_IP replaced by OBRS_CLIENT_IPS (now an regex-array to support multiple values)
* CHANGED Config option OBRS_SHIB_AFFILIATION replaced by OBRS_SHIB_AFFILIATIONS (now an array to support multiple values)
* FIXED If an user forgot to login a error is thrown when trying to start a recording.
* FIXED Error message is shown, if a shell script fails.
* OPTIONAL LDAP-Login variant (requires php-ldap).

---

## 2019-12-11
