<?php

/**
 * Picture
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH .  "/shared/includes.php");

/**
 * Get requested picture
 */
$image = basename($_SERVER['REQUEST_URI']);
if (preg_match('/^[a-zA-Z0-9]+\.png$/', $image) !== 1) {
	http_response_code(403);
	exit;
}
$image = preg_replace('/[^a-zA-Z0-9.]/', '', $image);

/**
 * Allow access from room clients
 */
$clientIP = $_SERVER['REMOTE_ADDR'];
if (\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS)) {
	/**
	 * Require content
	 */
	$file = OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_FINAL . '/' . $image;
	require_once(OBRS_BASE_PATH .  "/pages/picture.php");
}
http_response_code(403);
exit;
