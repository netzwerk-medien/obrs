<?php

/**
 * Setup frontend
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH .  "/shared/includes.php");
header("Location: " . OBRS_LINK_VIDEOARCHIVE);
