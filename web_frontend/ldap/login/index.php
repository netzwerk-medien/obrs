<?php

/**
 * LDAP Login frontend
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

$strings = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_LDAP);
if ($strings->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($strings->listErrors());
}
$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsGeneral->listErrors());
}
if (isset($_GET["dest"]) && !empty($_GET["dest"]) && preg_match("/^[a-zA-Z0-9\/]+$/", $_GET["dest"])) {
	if (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_LDAP) {
		if (isset($_POST["id"]) && !empty($_POST["id"]) && isset($_POST["pw"]) && !empty($_POST["pw"])) {
			$id = preg_replace(OBRS_LDAP_LOGIN_USERNAME_PATTERN, "", $_POST["id"]);
			if (strcmp($id, $_POST["id"]) !== 0) {
				echo '<a href="?dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($stringsGeneral->getString("ERROR"));
			}
			$loginId = preg_replace("/%1/", $id, OBRS_LDAP_LOGIN_USER);
			$pw = $_POST["pw"];
			$ds = ldap_connect(OBRS_LDAP_HOST) or die($stringsGeneral->getString("ERROR"));
			if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)) {
				echo '<a href="?dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($stringsGeneral->getString("ERROR"));
			} else if ($ds) {
				$bth = ldap_bind($ds, OBRS_LDAP_USER, OBRS_LDAP_PW) or die($stringsGeneral->getString("ERROR"));
				$query = preg_replace("/%1/", $id, OBRS_LDAP_LOGIN_SEARCH_USER_PATTERN);
				$res = ldap_search($ds, OBRS_LDAP_BASE, $query) or die($stringsGeneral->getString("ERROR"));
				$info = ldap_get_entries($ds, $res);
				$numberOfEntries = intval($info["count"]);
				if ($numberOfEntries > 0 && isset($info[0][OBRS_LDAP_ATTR_ID][0]) && !empty($info[0][OBRS_LDAP_ATTR_ID][0]) && strcmp($info[0][OBRS_LDAP_ATTR_ID][0], $id) === 0 && ldap_bind($ds, $loginId, $pw)) {
					$token = bin2hex(random_bytes(40));
					$username = $info[0][OBRS_LDAP_ATTR_NAME][0];
					$mail = $info[0][OBRS_LDAP_ATTR_MAIL][0];
					session_start();
					$params = session_get_cookie_params();
					setcookie("token", $token, time() + OBRS_AUTO_LOGOUT_TIME, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
					$_SESSION['token'] = $token;
					$_SESSION['mail'] = $mail;
					$_SESSION['userid'] = $id;
					$_SESSION['username'] = $username;
					$location = isset($_POST['to']) && !empty($_POST['to']) && is_string($_POST['to']) && strpos($_POST['to'], OBRS_LINK) === 0 ? preg_replace("/[?].*$/i", "", $_POST['to']) : $_GET["dest"];
					header("Location: $location");
				} else {
					echo '<a href="?dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
					die($stringsGeneral->getString("ERROR"));
				}
			}
		} else {
			session_start();
			$_SESSION = array();
			$params = session_get_cookie_params();
			setcookie(session_name(), "", time() - 1000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
			setcookie("token", "", time() - 1000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
			session_destroy();
			session_write_close();
			echo '<!DOCTYPE html>
			<html>
			<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>' . $strings->getString("TITLE") . '</title>
            <link rel="stylesheet" type="text/css" href="' . OBRS_WEB_CSS_MAIN . '">
            <link rel="stylesheet" type="text/css" href="' . OBRS_WEB_CSS_COMMON . '">
			<style>
                form, h1, div {margin: 0; margin-top: 50px; padding: 0; border: 0; width: 100%; text-align: center; }
                form > input {padding: 10px; background: white; font-size: 1.1em; border: 5px solid #234c5a; border-radius: 5px; margin: 15px; margin-bottom: 25px; max-width: calc( 100% - 70px )}
                form > input[type=submit], a.link {background: #234c5a; color: white; border: none; border-radius: 0px; text-transform: uppercase; padding: 15px; text-decoration: none; font-weight: bold;}
            </style>
			<script src="' . OBRS_WEB_JS_SCREENSAVER . '"></script>
			</head><body>
            <header><h1>' . $strings->getString("TITLE") . '</h1></header>
			<form method="post" action="?dest=' . rawurlencode($_GET['dest']) . '">';
			if (isset($_SERVER['HTTP_REFERER']) && strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH), parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) === false && strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH), parse_url(OBRS_LINK_USER_AREA, PHP_URL_PATH)) !== false) {
				echo '<input type="hidden" name="to" value="' . $_SERVER['HTTP_REFERER'] . '">';
			}
			echo $strings->getString("USERNAME") . ':<br><input name="id" autocomplete="off" type="text"><br>' . $strings->getString("PASSWORD") . ':<br><input name="pw" type="password"><br><input value="' . $strings->getString("SEND") . '" type="submit"></form>
			<script>if (screensaver) {
				screensaver.activate();
			}</script>
			</body>
			</html>';
		}
	} else {
		header("Location: " . $_GET['dest']);
	}
	exit;
}
echo $stringsGeneral->getString("ERROR");
http_response_code(403);
