function showElement(id, style) {
    if (typeof style !== "string") {
        style = "block";
    }
    var elem = document.getElementById(id);
    if (elem !== null) {
        elem.style.display = style;
    }
}

function hideElement(id) {
    var elem = document.getElementById(id);
    if (elem !== null) {
        elem.style.display = "none";
    }
}

function obrsCommand(profile, obrsFunction, additionalParam, retryOnError, boolParam, retry) {
    if (typeof obrsFunction != "function") {
        obrsFunction = function () {};
    }
    if (additionalParam == undefined || additionalParam == null) {
        additionalParam = 0;
    }
    if (retryOnError !== false) {
        retryOnError = true;
    }
    if (boolParam !== true) {
        boolParam = 0;
    } else {
        boolParam = 1;
    }
    if (retry == undefined || retry == null) {
        retry = 0;
    }
    const data = new FormData();
    data.append("profile", profile);
    data.append("additionalParam", additionalParam);
    data.append("boolParam", boolParam);
    fetch("/connector/index.php", {
        method: "POST",
        body: data,
        cache: "no-store"
    })
        .then(function (response) {
            return response.text();
        })
        .then(function (msg) {
            console.log("Profile sent: " + profile + ", received: " + msg);
            obrsFunction(msg);
        })
        .catch(function (error) {
            console.log("Request failed: " + error.statusText);
            if (retryOnError) {
                if (retry < 3) {
                    obrsCommand(profile, obrsFunction, additionalParam, retryOnError, boolParam, ++retry);
                } else {
                    showElement("errorMsg");
                }
            }
        });
}

function pingServer() {
    obrsCommand(4000, null, null, false);
}

window.addEventListener("load", function () {
    //Keep active
    window.setInterval(pingServer, 1800000);
});
