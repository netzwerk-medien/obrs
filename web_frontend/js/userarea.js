function showMoreNav() {
	var showMoreNavToggle = document.getElementById('topnav-toggle');
	if(showMoreNavToggle.classList.contains('shown')){
    	showMoreNavToggle.textContent = 'more_vert';
		showMoreNavToggle.classList.remove('shown');
		document.getElementById('morenav').classList.add('hidden');
	} else {
		showMoreNavToggle.textContent = 'close';
		showMoreNavToggle.classList.add('shown');
		document.getElementById('morenav').classList.remove('hidden');
	}
}
