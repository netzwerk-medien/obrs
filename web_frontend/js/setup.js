function currentSlide(id) {
    var slides = document.getElementsByClassName("slides");
    for (var i = 0; i < slides.length; i++) {
        if (slides[i].id == id) {
            var progress = document.querySelector("#progress");
            if (progress != null) {
                var percent = slides[i].dataset.progress;
                progress.style.width = percent + "vw";
                progress.style.backgroundColor = percent == 100 ? "darkGreen" : "";
            }
        }
        slides[i].style.display = "none";
    }
    showElement(id);
}

function resetSetup() {
    if (screensaver) {
        screensaver.deactivate();
    }
    profile = 0;
    background = 0;
    obrsCommand(0);
    currentSlide("background");
    getUserBackgrounds();
}

function setBackground(newBackground, pic) {
    background = newBackground;
    if (background == -2) {
        //green screen background
        profile += 20;
        obrsCommand(300, function (msg) {
            if (msg == 300) {
                setMedia(0);
            } else {
                document.getElementById("errorMsgInner").textContent = msg;
                showElement("errorMsg");
            }
        });
    } else if (background == -1) {
        //user background
        profile += 10;
        obrsCommand(
            5000,
            function (msg) {
                if (msg == 5000) {
                    currentSlide("media");
                } else {
                    document.getElementById("errorMsgInner").textContent = msg;
                    showElement("errorMsg");
                }
            },
            pic
        );
    } else if (background > 0) {
        //default or department background
        obrsCommand(background, function (msg) {
            if (msg == background) {
                currentSlide("media");
            } else {
                document.getElementById("errorMsgInner").textContent = msg;
                showElement("errorMsg");
            }
        });
    }
}

function getUserBackgrounds() {
    var lastCallUserBackgroundsLocal = Date.now();
    lastCallUserBackgrounds = lastCallUserBackgroundsLocal;
    document.querySelector("#background3inner").innerHTML = "";
    document.querySelector("#bg_button_user").classList.add("hidden");
    obrsCommand(
        5001,
        function (msg) {
            msg = JSON.parse(msg);
            if (msg.status === 0 && msg.files.length > 0) {
                document.querySelector("#bg_button_user").classList.remove("hidden");
                for (var bgC = 0; bgC < msg.files.length; bgC++) {
                    var bgImg = new Image();
                    bgImg.dataset.filename = msg.files[bgC];
                    bgImg.onclick = function (e) {
                        setBackground(-1, e.target.dataset.filename);
                    };
                    bgImg.onload = function (e) {
                        if (lastCallUserBackgrounds == lastCallUserBackgroundsLocal) {
                            var bgElem = document.createElement("div");
                            bgElem.className = "button middle picture";
                            bgElem.appendChild(e.target);
                            document.querySelector("#background3inner").appendChild(bgElem);
                        }
                    };
                    bgImg.src = msg.basePath + msg.files[bgC];
                }
            }
        },
        null,
        false
    );
}

function setMedia(media) {
    // Select media
    if (media == 2) {
        //laptop
        profile += 12;
    } else if (media == 1) {
        //document camera
        profile += 11;
    } else {
        //none
        profile += 10;
        if (background > 0) {
            background += 1000;
            obrsCommand(background, function (msg) {
                if (msg != background) {
                    document.getElementById("errorMsgInner").textContent = msg;
                    showElement("errorMsg");
                }
            });
        }
    }

    //All done, send profile to server
    console.log("Profile Index: " + profile);
    obrsCommand(profile, function (msg) {
        if (msg == profile) {
            currentSlide("final");
            if (screensaver) {
                screensaver.activate();
            }
        } else {
            document.getElementById("errorMsgInner").textContent = msg;
            showElement("errorMsg");
        }
    });
}

var profile, background, lastCallUserBackgrounds;

window.addEventListener("load", resetSetup);
