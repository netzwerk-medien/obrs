var screensaver = {
    screensaverElement: null,
    screensaverInterval: null,
    colorInterval: null,
    reset: function (thisObject) {
        if (thisObject.colorInterval) {
            clearInterval(thisObject.colorInterval);
        }
        if (thisObject.screensaverElement) {
            thisObject.screensaverElement.style.display = "none";
            thisObject.screensaverElement.textContent = "";
        }
    },
    init: function () {
        if (document.body) {
            this.screensaverElement = document.createElement("div");
            this.screensaverElement.style.content = " ";
            this.screensaverElement.style.position = "fixed";
            this.screensaverElement.style.zIndex = "10000";
            this.screensaverElement.style.margin = "0";
            this.screensaverElement.style.padding = "0";
            this.screensaverElement.style.top = "0";
            this.screensaverElement.style.left = "0";
            this.screensaverElement.style.width = "100vw";
            this.screensaverElement.style.height = "100vh";
            this.screensaverElement.style.display = "none";
            this.screensaverElement.style.alignItems = "center";
            this.screensaverElement.style.justifyContent = "center";
            this.screensaverElement.style.transition = "background-color 0.5s";
            this.screensaverElement.style.fontSize = "5em";
            this.screensaverElement.style.textShadow = "0px 0px 1px white";
            var thisObject = this;
            this.screensaverElement.onclick = function () {
                thisObject.reset(thisObject);
            };
            document.body.appendChild(this.screensaverElement);
        }
    },
    activate: function () {
        if (!this.screensaverElement) {
            this.init();
        }
        if (this.screensaverInterval) {
            clearInterval(this.screensaverInterval);
        }
        var thisObject = this;
        this.screensaverInterval = setInterval(function () {
            function randomColors() {
                function randomColor() {
                    return Math.round(Math.random() * 255);
                }
                function randomAlpha() {
                    return 0.95 + Math.random() / 20;
                }
                return "rgba(" + randomColor() + "," + randomColor() + "," + randomColor() + "," + randomAlpha() + ")";
            }
            if (thisObject.screensaverElement) {
                thisObject.screensaverElement.style.display = "flex";
                thisObject.colorInterval = setInterval(function () {
                    thisObject.screensaverElement.style.backgroundColor = randomColors();
                    if (thisObject.screensaverElement.textContent == "SCREEN") {
                        thisObject.screensaverElement.textContent = "SAVER";
                    } else if (thisObject.screensaverElement.textContent == "SAVER") {
                        thisObject.screensaverElement.textContent = "";
                    } else {
                        thisObject.screensaverElement.textContent = "SCREEN";
                    }
                }, 1500);
                setTimeout(function () {
                    thisObject.reset(thisObject);
                }, 15000);
            }
        }, 1200000);
    },
    deactivate: function () {
        if (this.screensaverInterval) {
            clearInterval(this.screensaverInterval);
        }
        this.reset(this);
    }
};
