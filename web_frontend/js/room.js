var timeDisplayElement = document.getElementById("time-display"),
    timeDisplayInterval,
    timeDisplayStartTime,
    countdownPeriod = 3,
    countdownInterval,
    previewTimeout,
    validateRecordingTimeout;

var countdown = countdownPeriod;

function startRecordingAfterCountDown() {
    if (screensaver) {
        screensaver.deactivate();
    }
    hideElement("record-button");
    hideElement("stop-button");
    hideElement("retry");
    hideElement("menu");
    showElement("countdown-display");
    document.getElementById("countdown-display").textContent = countdown;
    countdownInterval = window.setInterval(function () {
        countdown--;
        if (countdown > 0) {
            document.getElementById("countdown-display").textContent = countdown;
        } else {
            window.clearInterval(countdownInterval);
            countdown = countdownPeriod;
            hideElement("countdown-display");
            startRecording();
        }
    }, 1000);
}

function validateRecording() {
    if (validateRecordingTimeout != undefined && validateRecordingTimeout != null) {
        window.clearTimeout(validateRecordingTimeout);
    }
    validateRecordingTimeout = window.setTimeout(function () {
        obrsCommand(
            2002,
            function (msg) {
                if (msg != 2002) {
                    if (previewTimeout != undefined && previewTimeout != null) {
                        window.clearTimeout(previewTimeout);
                    }
                    document.getElementById("errorMsgInner").textContent = msg;
                    hideElement("main");
                    showElement("errorMsg");
                }
            }
        );
    }, 1000);
}

function startRecording() {
    function getTimeString(elapsed) {
        function addZeros(time) {
            if (time < 10) {
                return "0" + time;
            }
            return time;
        }
        var hours = addZeros(Math.floor(elapsed / 3600));
        var remaining = elapsed % 3600;
        var minutes = addZeros(Math.floor(remaining / 60));
        remaining = remaining % 60;
        var seconds = addZeros(Math.floor(remaining));
        return hours + ":" + minutes + ":" + seconds;
    }
    timeDisplayElement.textContent = getTimeString(0);
    obrsCommand(2000, function (msg) {
        if (msg == 2000) {
            if (validateRecordingActive) {
                validateRecording();
            }
            timeDisplayStartTime = Date.now();
            timeDisplayInterval = window.setInterval(function () {
                var elapsed = Date.now() - timeDisplayStartTime;
                timeDisplayElement.textContent = getTimeString(elapsed / 1000);
            }, 1000);
            showElement("stop-button", "inline-block");
            showElement("retry");
            timeDisplayElement.style.display = "";
        } else {
            document.getElementById("errorMsgInner").textContent = msg;
            hideElement("main");
            showElement("errorMsg");
        }
    });
}

function stopRecording(sendRequest) {
    if (validateRecordingTimeout != undefined && validateRecordingTimeout != null) {
        window.clearTimeout(validateRecordingTimeout);
    }
    if (sendRequest == undefined || sendRequest == null) {
        sendRequest = true;
    }
    window.clearInterval(timeDisplayInterval);
    timeDisplayElement.style.display = "none";

    if (sendRequest) {
        hideElement("retry");
        showElement("activityindicator", "inline-block");
        obrsCommand(2001, function (msg) {
            if (msg == 2001) {
                previewTimeout = window.setTimeout(function () {
                    obrsCommand(3000, function (msg) {
                        msg = JSON.parse(msg);
                        if (msg.status == 0) {
                            loadPreview(msg.message);
                        } else {
                            document.getElementById("errorMsgInner").textContent = msg.message;
                            hideElement("main");
                            showElement("errorMsg");
                        }
                    });
                }, 3000);
            } else {
                document.getElementById("errorMsgInner").textContent = msg;
                hideElement("main");
                showElement("errorMsg");
            }
        });
    }
}

function cancelRecording() {
    hideElement("retryConfirm");
    showElement("activityindicator", "inline-block");
    stopRecording(false);
    obrsCommand(2001, function (msg) {
        if (msg == 2001) {
            window.setTimeout(function () {
                obrsCommand(3002, function (msg) {
                    if (msg == 3002) {
                        hideElement("activityindicator");
                        startRecordingAfterCountDown();
                    } else {
                        document.getElementById("errorMsgInner").textContent = msg;
                        hideElement("main");
                        showElement("errorMsg");
                    }
                });
            }, 3000);
        } else {
            document.getElementById("errorMsgInner").textContent = msg;
            hideElement("main");
            showElement("errorMsg");
        }
    });
}

function cancelRecordingRequest() {
    hideElement("retry");
    showElement("retryConfirm");
}
function cancelRecordingCancel() {
    showElement("retry");
    hideElement("retryConfirm");
}

function loadPreview(videolink) {
    hideElement("record");
    showElement("video_preview");
    document.querySelector("#videosource").src = "/video/" + videolink;
    document.querySelector("#video").load();
}

function saveRecording() {
    hideElement("video_preview");
    obrsCommand(3001, function (msg) {
        if (msg == 3001) {
            window.location.reload();
        } else {
            document.getElementById("errorMsgInner").textContent = msg;
            hideElement("main");
            showElement("errorMsg");
        }
    });
}

function deleteRecording() {
    hideElement("video_preview");
    obrsCommand(3002, function (msg) {
        if (msg == 3002) {
            window.location.reload();
        } else {
            document.getElementById("errorMsgInner").textContent = msg;
            hideElement("main");
            showElement("errorMsg");
        }
    });
}

function resetRoom() {
    obrsCommand(0, function () {
        window.location.href = "../setup";
    });
}

if (screensaver) {
    screensaver.activate();
}
