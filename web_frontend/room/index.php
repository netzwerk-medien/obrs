<?php

/**
 * Room frontend
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

$strings = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_ROOM);
if ($strings->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($strings->listErrors());
}
$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($stringsGeneral->listErrors());
}

/**
 * Only allow access from room clients
 */
$clientIP = $_SERVER['REMOTE_ADDR'];
if (!\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS)) {
    http_response_code(403);
    die($stringsGeneral->getString('ERROR_FORBIDDEN'));
}

/**
 * Require content
 */
require_once(OBRS_BASE_PATH . "/pages/room.php");
