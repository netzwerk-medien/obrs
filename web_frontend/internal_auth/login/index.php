<?php

/**
 * Internal Login frontend
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

$strings = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_INTERNAL_AUTH);
if ($strings->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($strings->listErrors());
}
$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsGeneral->listErrors());
}
if (isset($_GET["dest"]) && !empty($_GET["dest"]) && preg_match("/^[a-zA-Z0-9\/]+$/", $_GET["dest"])) {
	if (OBRS_LOGIN_HANDLER == OBRS_LOGIN_HANDLER_INTERNAL) {
		if (isset($_POST["signup"]) && !empty($_POST["signup"]) && isset($_POST["id"]) && !empty($_POST["id"]) && isset($_POST["pw"]) && !empty($_POST["pw"]) && isset($_POST["confirmpw"]) && !empty($_POST["confirmpw"])) {
			/**
			 * Only allow access from setup client
			 */
			$clientIP = $_SERVER['REMOTE_ADDR'];
			if (!\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS)) {
				echo '<a href="?signup=1&amp;dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($strings->getString("IP_INVALID"));
			}
			$id = preg_replace(OBRS_INTERNAL_AUTH_LOGIN_USERNAME_PATTERN, "", $_POST["id"]);
			if (strcmp($id, $_POST["id"]) !== 0) {
				echo '<a href="?signup=1&amp;dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($strings->getString("USERNAME_INVALID"));
			}
			$pw = $_POST["pw"];
			if (strcmp($pw, $_POST["confirmpw"]) !== 0) {
				echo '<a href="?signup=1&amp;dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($strings->getString("PASSWORD_MISMATCH"));
			} else if (strlen($pw) < 8 || !preg_match("/[0-9]/si", $pw) || !preg_match("/[a-zA-Z]/si", $pw) || preg_match("/^[a-zA-Z0-9]+$/si", $pw)) {
				echo '<a href="?signup=1&amp;dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($strings->getString("PASSWORD_INVALID"));
			}
			$pdo = new \NetzwerkMedienObrs\Sqlite;
			$pdo->connect();
			if (!$pdo->existsInternalAuthUser($id)) {
				if ($pdo->insertInternalAuthUser($id, $pw)) {
					$location = rawurlencode($_GET["dest"]);
					header("Location: ?dest=$location");
				} else {
					echo '<a href="?signup=1&amp;dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
					die($stringsGeneral->getString("ERROR"));
				}
			} else {
				echo '<a href="?signup=1&amp;dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($strings->getString("USER_EXISTS"));
			}
		} else if ((!isset($_POST["signup"]) || empty($_POST["signup"])) && isset($_POST["id"]) && !empty($_POST["id"]) && isset($_POST["pw"]) && !empty($_POST["pw"])) {
			$id = preg_replace(OBRS_INTERNAL_AUTH_LOGIN_USERNAME_PATTERN, "", $_POST["id"]);
			if (strcmp($id, $_POST["id"]) !== 0) {
				echo '<a href="?dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($stringsGeneral->getString("ERROR"));
			}
			$pw = $_POST["pw"];
			$pdo = new \NetzwerkMedienObrs\Sqlite;
			$pdo->connect();
			if ($pdo->validateInternalAuthUser($id, $pw)) {
				$token = bin2hex(random_bytes(40));
				session_start();
				$params = session_get_cookie_params();
				setcookie("token", $token, time() + OBRS_AUTO_LOGOUT_TIME, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
				$_SESSION['token'] = $token;
				$_SESSION['mail'] = $id . "@local";
				$_SESSION['userid'] = $id;
				$_SESSION['username'] = $id;
				$location = isset($_POST['to']) && !empty($_POST['to']) && is_string($_POST['to']) && strpos($_POST['to'], OBRS_LINK) === 0 ? preg_replace("/[?].*$/i", "", $_POST['to']) : $_GET["dest"];
				header("Location: $location");
			} else {
				echo '<a href="?dest=' . rawurlencode($_GET["dest"]) . '">' . $strings->getString("ERROR_BACK") . '</a><br>';
				die($stringsGeneral->getString("ERROR"));
			}
		} else {
			session_start();
			$_SESSION = array();
			$params = session_get_cookie_params();
			setcookie(session_name(), "", time() - 1000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
			setcookie("token", "", time() - 1000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
			session_destroy();
			session_write_close();
			echo '<!DOCTYPE html>
			<html>
			<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>' . $strings->getString("TITLE") . '</title>
            <link rel="stylesheet" type="text/css" href="' . OBRS_WEB_CSS_MAIN . '">
            <link rel="stylesheet" type="text/css" href="' . OBRS_WEB_CSS_COMMON . '">
			<style>
                form, h1, div {margin: 0; margin-top: 50px; padding: 0; border: 0; width: 100%; text-align: center; }
                form > input {padding: 10px; background: white; font-size: 1.1em; border: 5px solid #234c5a; border-radius: 5px; margin: 15px; margin-bottom: 25px; max-width: calc( 100% - 70px )}
                form > input[type=submit], a.link {background: #234c5a; color: white; border: none; border-radius: 0px; text-transform: uppercase; padding: 15px; text-decoration: none; font-weight: bold;}
            </style>
			<script src="' . OBRS_WEB_JS_SCREENSAVER . '"></script>
			</head><body>';
			if (isset($_GET["signup"]) && $_GET["signup"] == 1) {
				echo '<header><h1>' . $strings->getString("SIGN_UP") . '</h1></header>
				<form method="post" action="?dest=' . rawurlencode($_GET['dest']) . '"><input type="hidden" name="signup" value="1">' . $strings->getString("USERNAME_SIGN_UP") . '<br><i>' . $strings->getString("USERNAME_SIGN_UP_PATTERN") . '</i><br><input required name="id" autocomplete="off" type="text"><br>' . $strings->getString("PASSWORD_SIGN_UP") . '<br><i>' . $strings->getString("PASSWORD_SIGN_UP_PATTERN") . '</i><br><input required name="pw" type="password"><br>' . $strings->getString("PASSWORD_CONFIRM_SIGN_UP") . '<br><input required name="confirmpw" type="password"><br><input value="' . $strings->getString("SEND_SIGN_UP") . '" type="submit"></form>';
			} else {
				echo '<header><h1>' . $strings->getString("TITLE") . '</h1></header>
				<form method="post" action="?dest=' . rawurlencode($_GET['dest']) . '">';
				if (isset($_SERVER['HTTP_REFERER']) && strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH), parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) === false && strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH), parse_url(OBRS_LINK_USER_AREA, PHP_URL_PATH)) !== false) {
					echo '<input type="hidden" name="to" value="' . $_SERVER['HTTP_REFERER'] . '">';
				}
				echo $strings->getString("USERNAME") . ':<br><input required name="id" autocomplete="off" type="text"><br>' . $strings->getString("PASSWORD") . ':<br><input required name="pw" type="password"><br><input value="' . $strings->getString("SEND") . '" type="submit"></form>';
				/**
				 * Only allow signup from setup client
				 */
				$clientIP = $_SERVER['REMOTE_ADDR'];
				if (\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS)) {
					echo '<div><a class="link" href="?dest=' . rawurlencode(rawurldecode($_GET['dest'])) . '&amp;signup=1">' . $strings->getString("SIGN_UP") . '</a></div>';
				}
			}
			echo '<script>if (screensaver) {
                screensaver.activate();
            }</script>
			</body></html>';
		}
	} else {
		header("Location: " . $_GET['dest']);
	}
	exit;
}
echo $stringsGeneral->getString("ERROR");
http_response_code(403);
