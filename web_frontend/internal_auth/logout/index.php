<?php

/**
 * Internal Logout frontend
 */

/**
 * OBRS Web Backend Path
 */
session_start();
$_SESSION = array();
$params = session_get_cookie_params();
setcookie(session_name(), "", time() - 1000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
setcookie("token", "", time() - 1000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
session_destroy();
session_write_close();
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");
$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($stringsGeneral->listErrors());
}

if (isset($_GET["dest"]) && !empty($_GET["dest"]) && preg_match("/^[a-zA-Z0-9\/]+$/", $_GET["dest"])) {
    $location = "../login/?dest=" . rawurlencode($_GET["dest"]);
    header("Location: $location");
    exit;
}
echo $stringsGeneral->getString("ERROR");
http_response_code(403);
