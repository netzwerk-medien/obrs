<?php

/**
 * Video
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH .  "/shared/includes.php");

/**
 * Get requested video
 */
$video = basename($_SERVER['REQUEST_URI']);
if (preg_match('/^[0-9]+\.' . OBRS_VIDEO_FILE_EXTENSION .  '$/', $video) !== 1) {
	http_response_code(403);
	exit;
}
$video = preg_replace('/[^a-zA-Z0-9.]/', '', $video);

/**
 * Allow access from room clients
 */
$clientIP = $_SERVER['REMOTE_ADDR'];
if (\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS)) {
	/**
	 * Require content
	 */
	require_once(OBRS_BASE_PATH .  "/pages/video.php");
	exit;
} else {
	/**
	 * Perform login
	 */
	require_once(OBRS_BASE_PATH .  "/class/ShibLogin.php");
	$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
	$shibSuccess = $shibConnection->login();
	if ($shibSuccess !== 0) {
		http_response_code(404);
		exit;
	}
	$email = $shibConnection->getMail();

	/**
	 * @var \NetzwerkMedienObrs\Sqlite Connect to the database
	 */
	$pdo = new \NetzwerkMedienObrs\Sqlite;
	$pdo->connect();
	$arr = $pdo->getVideoList($email);

	/**
	 * Filter video filename
	 */
	$videos = array();
	foreach ($arr as $arrVideo) {
		array_push($videos, $arrVideo['filename']);
	}

	/**
	 * Does video belong to user?
	 */
	if (in_array($video, $videos)) {
		/**
		 * Require content
		 */
		require_once(OBRS_BASE_PATH .  "/pages/video.php");
		exit;
	}
}
http_response_code(403);
exit;
