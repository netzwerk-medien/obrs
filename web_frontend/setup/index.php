<?php

/**
 * Setup frontend
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

$strings = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_SETUP);
if ($strings->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($strings->listErrors());
}
$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($stringsGeneral->listErrors());
}

/**
 * Only allow access from setup client
 */
$clientIP = $_SERVER['REMOTE_ADDR'];
if (!\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS)) {
    http_response_code(403);
    die($stringsGeneral->getString('ERROR_FORBIDDEN'));
}

session_start();

/**
 * Perform logout
 */
if (isset($_GET['logout']) && intval($_GET['logout']) == 1) {
    /**
     * @var \NetzwerkMedienObrs\Sqlite Connect to the database
     */
    $pdo = new \NetzwerkMedienObrs\Sqlite;
    $pdo->connect();
    $pdo->endCurrentSession();
    $pdo->endSessions();

    $scenesLocation = OBRS_SHELL_INTERFACE_PATH_SCENES;
    exec("DISPLAY=:0 $scenesLocation/clean.sh");
    exec("$scenesLocation/switchbackground.sh");
    exec("DISPLAY=:0 $scenesLocation/recordstop.sh");
    require_once(OBRS_BASE_PATH . "/tpl/login/obrs.php");
    exit;
}

/**
 * Perform login
 */
require_once(OBRS_BASE_PATH . "/class/ShibLogin.php");
$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
$shibSuccess = $shibConnection->login();
if ($shibSuccess !== 0) {
    switch ($shibSuccess) {
        case 2:
            require_once(OBRS_BASE_PATH . "/tpl/login/obrs.php");
            break;
        case 1:
            echo '<h2>' . $stringsGeneral->getString('ERROR_LOGIN') . '</h2><ul>';
            echo $shibConnection->getErrorMsg();
            echo '</ul><a href="./?logout=1">' . $stringsGeneral->getString('ERROR_LOGIN_BUTTON') . '</a>';
            break;
        default:
            echo $stringsGeneral->getString('ERROR_LOGIN_UNKNOWN', $shibSuccess);
    }
    exit;
}
$email = $shibConnection->getMail();

/**
 * From here logged in; require content
 */
require_once(OBRS_BASE_PATH . "/pages/setup.php");
