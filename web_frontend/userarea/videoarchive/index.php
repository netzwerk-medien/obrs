<?php

/**
 * Videoarchive frontend
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");
require_once(OBRS_BASE_PATH . '/class/UserAreaNavigation.php');

/**
 * Perform logout
 */
if (isset($_GET['logout']) && intval($_GET['logout']) == 1) {
    require_once(OBRS_BASE_PATH . "/tpl/login/userarea.php");
    exit;
}

$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($stringsGeneral->listErrors());
}
$stringsUserArea = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_USER_AREA);
if ($stringsUserArea->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($stringsUserArea->listErrors());
}

/**
 * Perform login
 */
require_once(OBRS_BASE_PATH . "/class/ShibLogin.php");
$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
$shibSuccess = $shibConnection->login();
if ($shibSuccess !== 0) {
    switch ($shibSuccess) {
        case 2:
            require_once(OBRS_BASE_PATH . "/tpl/login/userarea.php");
            break;
        case 1:
            echo '<h2>' . $stringsGeneral->getString('ERROR_LOGIN') . '</h2><ul>';
            echo $shibConnection->getErrorMsg();
            echo '</ul><a href="./?logout=1">' . $stringsGeneral->getString('ERROR_LOGIN_BUTTON') . '</a>';
            break;
        default:
            echo $stringsGeneral->getString('ERROR_LOGIN_UNKNOWN', $shibSuccess);
    }
    exit;
}
$email = $shibConnection->getMail();

/**
 * From here logged in; require content
 */
require_once(OBRS_BASE_PATH . "/pages/userarea-videoarchive.php");
