<?php

/**
 * Picture
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH .  "/shared/includes.php");

/**
 * Get requested picture
 */
$image = basename($_SERVER['REQUEST_URI']);
if (preg_match('/^[a-zA-Z0-9]+\.png$/', $image) !== 1) {
    http_response_code(403);
    exit;
}
$image = preg_replace('/[^a-zA-Z0-9.]/', '', $image);

/**
 * Perform login
 */
require_once(OBRS_BASE_PATH .  "/class/ShibLogin.php");
$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
$shibSuccess = $shibConnection->login();
if ($shibSuccess !== 0) {
    http_response_code(404);
    exit;
}
$email = $shibConnection->getMail();

/**
 * @var \NetzwerkMedienObrs\Sqlite Connect to the database
 */
$pdo = new \NetzwerkMedienObrs\Sqlite;
$pdo->connect();
$arr = array_merge($pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_FINAL), $pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_CROP));

/**
 * Filter picture filename
 */
foreach ($arr as $arrImg) {
    if (strcmp($arrImg['filename'], $image) === 0) {

        if ($arrImg['state'] == OBRS_USER_AREA_PICS_STATE_FINAL) {
            $file = OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_FINAL . '/' . $image;
        } else if ($arrImg['state'] == OBRS_USER_AREA_PICS_STATE_CROP) {
            $file = OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_CROP . '/' . $image;
        } else {
            http_response_code(403);
            exit;
        }

        /**
         * Require content
         */
        require_once(OBRS_BASE_PATH .  "/pages/picture.php");
        exit;
    }
}
http_response_code(403);
exit;
