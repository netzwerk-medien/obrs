<?php

/**
 * Userarea Pics upload
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

function errorExit($text)
{
	require_once(OBRS_BASE_PATH . "/class/UserareaErrorTemplate.php");
	$errorText = new \NetzwerkMedienObrs\UserareaErrorTemplate($text);
	$errorText->getHTML();
	exit;
}

$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsGeneral->listErrors());
}

/**
 * Perform login
 */
require_once(OBRS_BASE_PATH . "/class/ShibLogin.php");
$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
$shibSuccess = $shibConnection->login();
if ($shibSuccess !== 0) {
	switch ($shibSuccess) {
		case 2:
			require_once(OBRS_BASE_PATH . "/tpl/login/userarea.php");
			break;
		case 1:
			echo '<h2>' . $stringsGeneral->getString('ERROR_LOGIN') . '</h2><ul>';
			echo $shibConnection->getErrorMsg();
			echo '</ul><a href="./?logout=1">' . $stringsGeneral->getString('ERROR_LOGIN_BUTTON') . '</a>';
			break;
		default:
			echo $stringsGeneral->getString('ERROR_LOGIN_UNKNOWN', $shibSuccess);
	}
	exit;
}
$email = $shibConnection->getMail();

$stringsUserAreaPics = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_USER_AREA_PICS);
if ($stringsUserAreaPics->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsUserAreaPics->listErrors());
}

/**
 * @var \NetzwerkMedienObrs\Sqlite Connect to the database
 */
$pdo = new \NetzwerkMedienObrs\Sqlite;
if ($pdo->connect()) {
	if (!isset($_FILES['bilder'])) {
		errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_NONE'));
	}
	$numberOfPics = count($_FILES['bilder']['tmp_name']);
	$arrayOfPics = array();
	$cropPics = false;

	if ($numberOfPics < 1) {
		errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_NONE'));
	}
	if (count($pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_FINAL)) + count($pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_CROP)) + $numberOfPics > OBRS_USER_AREA_PICS_MAX_NUMBER) {
		errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_TOO_MANY', OBRS_USER_AREA_PICS_MAX_NUMBER));
	}
	for ($i = 0; $i < $numberOfPics; $i++) {

		$arrayOfPics[$i] = array();

		$filenameTmp = $_FILES['bilder']['tmp_name'][$i];
		if (empty($filenameTmp)) {
			errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_NO_FILE'));
		}

		if ($_FILES['bilder']['size'][$i] > OBRS_USER_AREA_PICS_MAX_SIZE * 1048576) {
			errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_TOO_HUGE'));
		}

		$arrayOfPics[$i]["filenameTmp"] = $filenameTmp;

		$imgmeta = getimagesize($filenameTmp);
		if (!$imgmeta) {
			errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_NO_IMAGE'));
		}
		$type = $imgmeta["mime"];
		if ($type != "image/jpeg" && $type != "image/png") {
			errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_NO_IMAGE'));
		}
		$arrayOfPics[$i]["mime"] = $type;

		list($width, $height) = $imgmeta;
		if ($width < OBRS_USER_AREA_PICS_WIDTH || $height < OBRS_USER_AREA_PICS_HEIGHT) {
			errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_TOO_SMALL'));
		}
		$arrayOfPics[$i]["width"] = $width;
		$arrayOfPics[$i]["height"] = $height;

		if ($width / $height < OBRS_USER_AREA_PICS_WIDTH / OBRS_USER_AREA_PICS_HEIGHT) {
			$newwidth = OBRS_USER_AREA_PICS_WIDTH;
			$newheight = intval($newwidth * $arrayOfPics[$i]["height"] / $arrayOfPics[$i]["width"]);
		} else {
			$newheight = OBRS_USER_AREA_PICS_HEIGHT;
			$newwidth = intval($newheight * $arrayOfPics[$i]["width"] / $arrayOfPics[$i]["height"]);
		}
		if ($newwidth < OBRS_USER_AREA_PICS_WIDTH || $newheight < OBRS_USER_AREA_PICS_HEIGHT) {
			errorExit($stringsUserAreaPics->getString('BACKEND_UPLOAD_TOO_SMALL'));
		}
		$arrayOfPics[$i]["newwidth"] = $newwidth;
		$arrayOfPics[$i]["newheight"] = $newheight;

		$id = bin2hex(random_bytes(15));
		$arrayOfPics[$i]["id"] = $id;
	}
	for ($i = 0; $i < count($arrayOfPics); $i++) {

		$fileTmp = OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_TMP . "/" . $arrayOfPics[$i]["id"];
		if (!$pdo->insertBackground($email, OBRS_USER_AREA_PICS_STATE_TMP, $arrayOfPics[$i]["id"] . '.png')) {
			errorExit($stringsUserAreaPics->getString('BACKEND_INTERNAL_ERROR'));
		}

		move_uploaded_file($arrayOfPics[$i]["filenameTmp"], $fileTmp);

		$imgd = imagecreatetruecolor($arrayOfPics[$i]["newwidth"], $arrayOfPics[$i]["newheight"]);
		if ($arrayOfPics[$i]["mime"] == "image/jpeg") {
			$imgr = imagecreatefromjpeg($fileTmp);
		} else if ($arrayOfPics[$i]["mime"] == "image/png") {
			$imgr = imagecreatefrompng($fileTmp);
		}

		unlink($fileTmp);

		$resizesuc = imagecopyresampled($imgd, $imgr, 0, 0, 0, 0, $arrayOfPics[$i]["newwidth"], $arrayOfPics[$i]["newheight"], $arrayOfPics[$i]["width"], $arrayOfPics[$i]["height"]);
		if (!$resizesuc) {
			errorExit($stringsUserAreaPics->getString('BACKEND_INTERNAL_ERROR'));
		}
		if ($arrayOfPics[$i]["newwidth"] != OBRS_USER_AREA_PICS_WIDTH || $arrayOfPics[$i]["newheight"] != OBRS_USER_AREA_PICS_HEIGHT) {
			if (!$pdo->updateBackgroundState($email, OBRS_USER_AREA_PICS_STATE_CROP, $arrayOfPics[$i]["id"] . '.png')) {
				errorExit($stringsUserAreaPics->getString('BACKEND_INTERNAL_ERROR'));
			}
			$cropPics = true;
			imagepng($imgd, OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_CROP . "/" . $arrayOfPics[$i]["id"] . ".png", 6);
		} else {
			if (!$pdo->updateBackgroundState($email, OBRS_USER_AREA_PICS_STATE_FINAL, $arrayOfPics[$i]["id"] . '.png')) {
				errorExit($stringsUserAreaPics->getString('BACKEND_INTERNAL_ERROR'));
			}
			imagepng($imgd, OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_FINAL . "/" . $arrayOfPics[$i]["id"] . ".png", 6);
		}
		imagedestroy($imgr);
		imagedestroy($imgd);
	}
	if ($cropPics) {
		header('Location: ./crop/');
	} else {
		header('Location: ./manage/');
	}
} else {
	errorExit($stringsUserAreaPics->getString('BACKEND_SERVER_ERROR'));
}
