<?php

/**
 * Userarea Pics crop
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

function errorExit($text)
{
	require_once(OBRS_BASE_PATH . "/class/UserareaErrorTemplate.php");
	$errorText = new \NetzwerkMedienObrs\UserareaErrorTemplate($text);
	$errorText->getHTML();
	exit;
}

$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsGeneral->listErrors());
}

/**
 * Perform login
 */
require_once(OBRS_BASE_PATH . "/class/ShibLogin.php");
$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
$shibSuccess = $shibConnection->login();
if ($shibSuccess !== 0) {
	switch ($shibSuccess) {
		case 2:
			require_once(OBRS_BASE_PATH . "/tpl/login/userarea.php");
			break;
		case 1:
			echo '<h2>' . $stringsGeneral->getString('ERROR_LOGIN') . '</h2><ul>';
			echo $shibConnection->getErrorMsg();
			echo '</ul><a href="./?logout=1">' . $stringsGeneral->getString('ERROR_LOGIN_BUTTON') . '</a>';
			break;
		default:
			echo $stringsGeneral->getString('ERROR_LOGIN_UNKNOWN', $shibSuccess);
	}
	exit;
}
$email = $shibConnection->getMail();

$stringsUserAreaPics = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_USER_AREA_PICS);
if ($stringsUserAreaPics->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsUserAreaPics->listErrors());
}

/**
 * @var \NetzwerkMedienObrs\Sqlite Connect to the database
 */
$pdo = new \NetzwerkMedienObrs\Sqlite;
if ($pdo->connect()) {

	if (!isset($_POST["pic"]) || empty($_POST["pic"]) || preg_match('/^[0-9a-zA-Z]+\.png$/', $_POST["pic"]) !== 1) {
		errorExit($stringsUserAreaPics->getString('BACKEND_CROP_NO_IMAGE'));
	}

	$id = $_POST["pic"];
	$pic = OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_CROP . "/$id";

	if (!$pdo->doesBackgroundBelongToUser($email, $id)) {
		errorExit($stringsUserAreaPics->getString('BACKEND_CROP_NO_FILE'));
	}
	if (!file_exists($pic)) {
		errorExit($stringsUserAreaPics->getString('BACKEND_CROP_NO_FILE'));
	}

	$imgmeta = getimagesize($pic);

	list($width, $height) = $imgmeta;
	$srcWidth = $width;
	$srcHeight = $height;
	$picsTop = 0;
	$picsLeft = 0;
	if ($width / $height < OBRS_USER_AREA_PICS_WIDTH / OBRS_USER_AREA_PICS_HEIGHT) {
		$picsTop = intval($_POST["pics-top"]);
		$srcHeight = $height - $picsTop;
		if ($srcHeight < OBRS_USER_AREA_PICS_HEIGHT) {
			$picsTop += ($srcHeight - OBRS_USER_AREA_PICS_HEIGHT);
		}
		if ($picsTop < 0) {
			$picsTop = 0;
		}
	} else {
		$picsLeft = intval($_POST["pics-left"]);
		$srcWidth = $width - $picsLeft;
		if ($srcWidth < OBRS_USER_AREA_PICS_WIDTH) {
			$picsLeft += ($srcWidth - OBRS_USER_AREA_PICS_WIDTH);
		}
		if ($picsLeft < 0) {
			$picsLeft = 0;
		}
	}
	$imgd = imagecreatetruecolor(OBRS_USER_AREA_PICS_WIDTH, OBRS_USER_AREA_PICS_HEIGHT);
	$imgr = imagecreatefrompng($pic);
	$resizesuc = imagecopyresampled($imgd, $imgr, 0, 0, $picsLeft, $picsTop, OBRS_USER_AREA_PICS_WIDTH, OBRS_USER_AREA_PICS_HEIGHT, OBRS_USER_AREA_PICS_WIDTH, OBRS_USER_AREA_PICS_HEIGHT);
	if (!$resizesuc) {
		errorExit($stringsUserAreaPics->getString('BACKEND_INTERNAL_ERROR'));
	}
	if (!$pdo->updateBackgroundState($email, OBRS_USER_AREA_PICS_STATE_FINAL, $id)) {
		errorExit($stringsUserAreaPics->getString('BACKEND_INTERNAL_ERROR'));
	}
	imagepng($imgd, OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_FINAL . "/$id", 6);
	imagedestroy($imgr);
	imagedestroy($imgd);
	unlink("$pic");
	if (count($pdo->getBackgroundList($email, OBRS_USER_AREA_PICS_STATE_CROP)) > 0) {
		header('Location: ./');
	} else {
		header('Location: ../');
	}
}
