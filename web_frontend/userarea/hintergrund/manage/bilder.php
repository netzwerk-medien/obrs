<?php

/**
 * Userarea Pics manage
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

function errorExit($text)
{
	require_once(OBRS_BASE_PATH . "/class/UserareaErrorTemplate.php");
	$errorText = new \NetzwerkMedienObrs\UserareaErrorTemplate($text);
	$errorText->getHTML();
	exit;
}

$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsGeneral->listErrors());
}

/**
 * Perform login
 */
require_once(OBRS_BASE_PATH . "/class/ShibLogin.php");
$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
$shibSuccess = $shibConnection->login();
if ($shibSuccess !== 0) {
	switch ($shibSuccess) {
		case 2:
			require_once(OBRS_BASE_PATH . "/tpl/login/userarea.php");
			break;
		case 1:
			echo '<h2>' . $stringsGeneral->getString('ERROR_LOGIN') . '</h2><ul>';
			echo $shibConnection->getErrorMsg();
			echo '</ul><a href="./?logout=1">' . $stringsGeneral->getString('ERROR_LOGIN_BUTTON') . '</a>';
			break;
		default:
			echo $stringsGeneral->getString('ERROR_LOGIN_UNKNOWN', $shibSuccess);
	}
	exit;
}
$email = $shibConnection->getMail();

$stringsUserAreaPics = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_USER_AREA_PICS);
if ($stringsUserAreaPics->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($stringsUserAreaPics->listErrors());
}

/**
 * @var \NetzwerkMedienObrs\Sqlite Connect to the database
 */
$pdo = new \NetzwerkMedienObrs\Sqlite;
if ($pdo->connect()) {

	$modes = array('delete');

	if (!isset($_POST["mode"]) || empty($_POST["mode"]) || !in_array($_POST["mode"], $modes)) {
		errorExit($stringsUserAreaPics->getString('BACKEND_MANAGE_OPERATION_ERROR'));
	}
	switch ($_POST["mode"]) {
		case 'delete':
			if (!isset($_POST["delete"]) || empty($_POST["delete"]) || preg_match('/^[0-9a-zA-Z]+\.png$/', $_POST["delete"]) !== 1) {
				errorExit($stringsUserAreaPics->getString('BACKEND_MANAGE_DELETE_NO_IMAGE'));
			}

			$id = $_POST["delete"];
			$pic = OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_FINAL . "/$id";

			if (!$pdo->doesBackgroundBelongToUser($email, $id)) {
				errorExit($stringsUserAreaPics->getString('BACKEND_MANAGE_DELETE_NO_FILE'));
			}
			if (!file_exists($pic)) {
				errorExit($stringsUserAreaPics->getString('BACKEND_MANAGE_DELETE_NO_FILE'));
			}
			if (!$pdo->updateBackgroundState($email, OBRS_USER_AREA_PICS_STATE_TRASH, $id)) {
				errorExit($stringsUserAreaPics->getString('BACKEND_INTERNAL_ERROR'));
			}
			if (!rename("$pic", OBRS_SHELL_MEDIA_PATH_BACKGROUNDS_USER_TRASH . "/$id")) {
				errorExit($stringsUserAreaPics->getString('BACKEND_MANAGE_DELETE_ERROR'));
			}
			break;
	}


	header('Location: ./');
}
