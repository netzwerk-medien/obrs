<?php
/**
 * Videomanager frontend
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");
require_once(OBRS_BASE_PATH . '/class/AdminAreaNavigation.php');

/**
 * Perform logout
 */
if (isset($_GET['logout']) && intval($_GET['logout']) == 1) {
    require_once(OBRS_BASE_PATH . "/tpl/login/adminarea.php");
    exit;
}

$stringsGeneral = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_GENERAL);
if ($stringsGeneral->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($stringsGeneral->listErrors());
}
$stringsAdminArea = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_ADMIN_AREA);
if ($stringsAdminArea->hasErrors()) {
    echo OBRS_I18N_ERROR;
    throw new \Exception($stringsAdminArea->listErrors());
}

/**
 * Perform login
 */
require_once(OBRS_BASE_PATH . "/class/ShibLogin.php");
$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
$shibSuccess = $shibConnection->login();
if ($shibSuccess !== 0) {
    switch ($shibSuccess) {
        case 2:
            require_once(OBRS_BASE_PATH . "/tpl/login/adminarea.php");
            break;
        case 1:
            echo '<h2>' . $stringsGeneral->getString('ERROR_LOGIN') . '</h2><ul>';
            echo $shibConnection->getErrorMsg();
            echo '</ul><a href="./?logout=1">' . $stringsGeneral->getString('ERROR_LOGIN_BUTTON') . '</a>';
            break;
        default:
            echo $stringsGeneral->getString('ERROR_LOGIN_UNKNOWN', $shibSuccess);
    }
    exit;
}
$email = $shibConnection->getMail();

/**
 * Check admin
 */
if (empty($email) || !defined("OBRS_ADMIN_LEVEL_VIDEO_MGR") || !\NetzwerkMedienObrs\checkAdmin($email, OBRS_ADMIN_LEVEL_VIDEO_MGR)) {
    echo $stringsAdminArea->getString('NO_ADMIN');
    exit;
}

/**
 * From here logged in; require content
 */
require_once(OBRS_BASE_PATH . "/pages/adminarea-videomanager.php");
