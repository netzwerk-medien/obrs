<?php

/**
 * AJAX Frontend for Client requests
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH . "/shared/includes.php");

/**
 * Set strings
 */
$strings = new \NetzwerkMedienObrs\StringFinder(OBRS_I18N_PATH, OBRS_I18N_LOCALE, OBRS_I18N_SET_CONNECTOR);
if ($strings->hasErrors()) {
	echo OBRS_I18N_ERROR;
	throw new \Exception($strings->listErrors());
}

/**
 * Is profile set?
 */
if (!isset($_POST['profile'])) {
	echo $strings->getString('REQUEST');
	exit;
}

/**
 * @var int Profile specifying task to perform
 */
$profile = intval($_POST['profile']);

/**
 * Only allow access from setup and room clients
 */
$clientIP = $_SERVER['REMOTE_ADDR'];
if (!\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS) || $profile < 0) {
	echo $strings->getString('REQUEST');
	exit;
}

/**
 * @var string Scenes script Location
 */
$scenesLocation = OBRS_SHELL_INTERFACE_PATH_SCENES;

/**
 * From here authorized; require content
 */
require_once(OBRS_BASE_PATH . "/pages/connector.php");
