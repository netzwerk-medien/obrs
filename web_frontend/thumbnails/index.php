<?php

/**
 * Thumbnail
 */

/**
 * OBRS Web Backend Path
 */
define("OBRS_BASE_PATH", "/opt/obrsapp/web_backend");
require_once(OBRS_BASE_PATH .  "/shared/includes.php");

/**
 * Get requested video
 */
$thumbnail = basename($_SERVER['REQUEST_URI']);
if (preg_match('/^[0-9]+\.jpg$/', $thumbnail) !== 1) {
	http_response_code(403);
	exit;
}
$thumbnail = preg_replace('/[^a-zA-Z0-9.]/', '', $thumbnail);

/**
 * Allow access from room clients
 */
$clientIP = $_SERVER['REMOTE_ADDR'];
if (\NetzwerkMedienObrs\validateClientIP($clientIP, OBRS_CLIENT_IPS)) {
	/**
	 * Require content
	 */
	require_once(OBRS_BASE_PATH .  "/pages/thumbnails.php");
	exit;
} else {
	/**
	 * Perform login
	 */
	require_once(OBRS_BASE_PATH .  "/class/ShibLogin.php");
	$shibConnection = new \NetzwerkMedienObrs\ShibLogin($_SERVER);
	$shibSuccess = $shibConnection->login();
	if ($shibSuccess !== 0) {
		http_response_code(404);
		exit;
	}
	$email = $shibConnection->getMail();

	/**
	 * @var \NetzwerkMedienObrs\Sqlite Connect to the database
	 */
	$pdo = new \NetzwerkMedienObrs\Sqlite;
	$pdo->connect();
	$arr = $pdo->getVideoList($email);

	/**
	 * Filter thumbnail filename
	 */
	$thumbnails = array();
	foreach ($arr as $video) {
		array_push($thumbnails, preg_replace('/[.]' . OBRS_VIDEO_FILE_EXTENSION . '$/', '.jpg', $video['filename']));
	}

	/**
	 * Does thumbnail belong to user?
	 */
	if (in_array($thumbnail, $thumbnails)) {
		/**
		 * Require content
		 */
		require_once(OBRS_BASE_PATH .  "/pages/thumbnails.php");
		exit;
	}
}
http_response_code(403);
exit;
